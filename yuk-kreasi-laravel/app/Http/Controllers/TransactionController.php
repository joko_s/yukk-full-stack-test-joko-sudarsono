<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Core\Foundation\Http\Response\HttpCode;
use App\Core\Foundation\Http\Response\DataSerializer;
use App\Core\Foundation\Http\Response\SingleDataSerializer;

use App\Core\Modules\Transaction\Service\TransactionService;
use App\Core\Modules\Transaction\Request\CreateTransactionRequest;
use App\Core\Modules\Transaction\Request\UpdateTransactionRequest;
use App\Core\Modules\Transaction\Response\CreateTransactionResponse;
use App\Core\Modules\Transaction\Response\GetListTransactionResponse;
use App\Core\Modules\Transaction\Response\ShowTransactionResponse;
use App\Core\Modules\Transaction\Response\UpdateTransactionResponse;

class TransactionController extends Controller
{
    /**
     * @var TransactionService
     */
    protected TransactionService $service;

    /**
     * Constructor
     * @param TransactionService $service
     */
    public function __construct(TransactionService $service)
    {
        $this->service = $service;
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $data = $this->service->getList($request->all());

        return response()->json(
            fractal(
                $data,
                new GetListTransactionResponse,
                new DataSerializer($data)
            ),
            HttpCode::OK
        );
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function show(Request $request, $id): JsonResponse
    {
        $data = $this->service->show($id);

        return response()->json(
            fractal(
                $data,
                new ShowTransactionResponse,
                new SingleDataSerializer($data)
            ),
            HttpCode::OK
        );
    }

    /**
     *
     * @param CreateTransactionRequest $request
     * @return JsonResponse
     */
    public function create(CreateTransactionRequest $request): JsonResponse
    {
        $data = $this->service->create($request->all());

        return response()->json(
            fractal(
                $data,
                new CreateTransactionResponse,
                new SingleDataSerializer($data)
            ),
            HttpCode::OK
        );
    }

    /**
     *
     * @param UpdateTransactionRequest $request
     * @return JsonResponse
     */
    public function update(UpdateTransactionRequest $request): JsonResponse
    {
        $data = $this->service->update($request->all());

        return response()->json(
            fractal(
                $data,
                new UpdateTransactionResponse,
                new SingleDataSerializer($data)
            ),
            HttpCode::OK
        );
    }
}
