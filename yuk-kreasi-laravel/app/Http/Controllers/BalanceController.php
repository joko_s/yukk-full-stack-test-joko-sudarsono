<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Core\Foundation\Http\Response\HttpCode;
use App\Core\Foundation\Http\Response\DataSerializer;
use App\Core\Foundation\Http\Response\SingleDataSerializer;

use App\Core\Modules\Balance\Service\BalanceService;
use App\Core\Modules\Balance\Response\GetBalanceResponse;
use App\Core\Modules\Balance\Response\GetBalanceHistoriesResponse;

class BalanceController extends Controller
{
    /**
     * @var BalanceService
     */
    protected BalanceService $service;

    /**
     * Constructor
     * @param BalanceService $service
     */
    public function __construct(BalanceService $service)
    {
        $this->service = $service;
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function balance(Request $request): JsonResponse
    {
        $data = $this->service->balance($request->all());

        return response()->json(
            fractal(
                $data,
                new GetBalanceResponse,
                new SingleDataSerializer($data)
            ),
            HttpCode::OK
        );
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function histories(Request $request): JsonResponse
    {
        $data = $this->service->histories($request->all());

        return response()->json(
            fractal(
                $data,
                new GetBalanceHistoriesResponse,
                new DataSerializer($data)
            ),
            HttpCode::OK
        );
    }
}
