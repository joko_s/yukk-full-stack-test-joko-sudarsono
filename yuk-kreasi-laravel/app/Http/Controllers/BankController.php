<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Core\Foundation\Http\Response\HttpCode;
use App\Core\Foundation\Http\Response\DataSerializer;
use App\Core\Foundation\Http\Response\SingleDataSerializer;

use App\Core\Modules\Bank\Service\BankServiceInterface;
use App\Core\Modules\Bank\Response\GetBankListResponse;

class BankController extends Controller
{
    /**
     * @var BankServiceInterface
     */
    protected BankServiceInterface $service;

    /**
     * Constructor
     * @param BankServiceInterface $service
     */
    public function __construct(BankServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $data = $this->service->getList($request->all());

        return response()->json(
            fractal(
                $data,
                new GetBankListResponse,
                new DataSerializer($data)
            ),
            HttpCode::OK
        );
    }
}
