<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Core\Foundation\Http\Response\HttpCode;
use App\Core\Foundation\Http\Response\DataSerializer;
use App\Core\Foundation\Http\Response\SingleDataSerializer;

use App\Core\Modules\User\Service\UserServiceInterface;
use App\Core\Modules\User\Request\LoginRequest;
use App\Core\Modules\User\Request\RegisterRequest;

use App\Core\Modules\User\Response\UserLoginResponse;
use App\Core\Modules\User\Response\UserRegisterResponse;
use App\Core\Modules\User\Response\UserLogoutResponse;

class UserController extends Controller
{
    /**
     * @var UserServiceInterface
     */
    protected UserServiceInterface $service;

    /**
     * Constructor
     * @param UserServiceInterface $service
     */
    public function __construct(UserServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $data = $this->service->login($request->all());

        return response()->json(
            fractal(
                $data,
                new UserLoginResponse,
                new SingleDataSerializer($data)
            ),
            HttpCode::OK
        );
    }

    /**
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $data = $this->service->register($request->all());

        return response()->json(
            fractal(
                $data,
                new UserRegisterResponse,
                new SingleDataSerializer($data)
            ),
            HttpCode::OK
        );
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $data = $this->service->logout($request->all());

        return response()->json(
            fractal(
                $data,
                new UserLogoutResponse,
                new SingleDataSerializer($data)
            ),
            HttpCode::OK
        );
    }
}
