<?php

namespace App\Core\Modules\Transaction;

use Illuminate\Support\ServiceProvider;

use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionHistoryInterface;
use App\Core\Modules\Transaction\Entity\Model\Transaction;
use App\Core\Modules\Transaction\Entity\Model\TransactionHistory;

use App\Core\Modules\Transaction\Entity\Repository\Contract\TransactionRepositoryInterface;
use App\Core\Modules\Transaction\Entity\Repository\Contract\TransactionHistoryRepositoryInterface;
use App\Core\Modules\Transaction\Entity\Repository\TransactionRepository;
use App\Core\Modules\Transaction\Entity\Repository\TransactionHistoryRepository;

use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceRepositoryInterface;
use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceHistoryRepositoryInterface;
use App\Core\Modules\Balance\Entity\Repository\BalanceRepository;
use App\Core\Modules\Balance\Entity\Repository\BalanceHistoryRepository;

use App\Core\Modules\Transaction\Service\Action\Contract\CreateTransactionActionInterface;
use App\Core\Modules\Transaction\Service\Action\Contract\GetListTransactionActionInterface;
use App\Core\Modules\Transaction\Service\Action\Contract\ShowTransactionActionInterface;
use App\Core\Modules\Transaction\Service\Action\Contract\UpdateTransactionActionInterface;
use App\Core\Modules\Transaction\Service\Action\CreateTransactionAction;
use App\Core\Modules\Transaction\Service\Action\GetListTransactionAction;
use App\Core\Modules\Transaction\Service\Action\ShowTransactionAction;
use App\Core\Modules\Transaction\Service\Action\UpdateTransactionAction;

use App\Core\Modules\Transaction\Service\TransactionServiceInterface;
use App\Core\Modules\Transaction\Service\TransactionService;

/**
 * Class TransactionProvider
 *
 */
class TransactionProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {
        // entity
        $this->app->bind(TransactionInterface::class, Transaction::class);
        $this->app->bind(TransactionHistoryInterface::class, TransactionHistory::class);

        // repository
        $this->app->bind(TransactionRepositoryInterface::class, TransactionRepository::class);
        $this->app->bind(TransactionHistoryRepositoryInterface::class, TransactionHistoryRepository::class);
        $this->app->bind(BalanceRepositoryInterface::class, BalanceRepository::class);
        $this->app->bind(BalanceHistoryRepositoryInterface::class, BalanceHistoryRepository::class);

        // Action
        $this->app->bind(CreateTransactionActionInterface::class, CreateTransactionAction::class);
        $this->app->bind(GetListTransactionActionInterface::class, GetListTransactionAction::class);
        $this->app->bind(ShowTransactionActionInterface::class, ShowTransactionAction::class);
        $this->app->bind(UpdateTransactionActionInterface::class, UpdateTransactionAction::class);

        // Service
        $this->app->bind(TransactionServiceInterface::class, TransactionService::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            // entity
            Transaction::class,
            TransactionHistory::class,

            // repository
            TransactionRepository::class,
            TransactionHistoryRepository::class,
            BalanceRepository::class,
            BalanceHistoryRepository::class,

            // Action
            CreateTransactionAction::class,
            GetListTransactionAction::class,
            ShowTransactionAction::class,
            UpdateTransactionAction::class,

            // Service
            TransactionService::class,
        ];
    }
}
