<?php

namespace App\Core\Modules\Transaction\Service\Action;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

use App\Core\Modules\Balance\Entity\Model\Contract\BalanceHistoryInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;
use App\Core\Modules\Transaction\Service\AbstractTransactionService;
use App\Core\Modules\Transaction\Service\Action\Contract\UpdateTransactionActionInterface;
use App\Core\Modules\Transaction\Exception\TransactionException;

/**
 * Class UpdateTransactionAction
 *
 */
class UpdateTransactionAction extends AbstractTransactionService implements UpdateTransactionActionInterface 
{
    /**
     *
     * @param array $input
     * @return array|null
     */
    public function handle(array $input): ?TransactionInterface
    {
        // validate transaction
        $id = data_get($input, 'transaction_id');
        $this->validateTransaction($id, data_get($input, 'status'));

        try {

            return DB::transaction(function () use ($input, $id) {
                // build payload
                $now = \Carbon\Carbon::now();
                $credentials = auth()->user();
                $status = data_get($input, 'status');
                $notes = data_get($input, 'notes');

                $transaction = $this->transactionRepository->findTransactionLock($id, $credentials);
                $isTopup = $transaction->getTransactionType() == TransactionInterface::TRANSACTION_TYPE_TOPUP;

                // save history
                $history = $this->saveHistory([
                    'transaction_id' => $transaction->getID(),
                    'status' => $status,
                    'notes' => $notes,
                    'created_at' => $now,
                ]);

                // increate balance if transaction type is 'topup' and status settled
                $topupSettled = $isTopup && $status == TransactionInterface::STATUS_SETTLED;
                $transactionRejected = ! $isTopup && $status == TransactionInterface::STATUS_REJECTED;
                if ($topupSettled || $transactionRejected) {
                    // get balance with lockForUpdate
                    $balance = $this->balanceRepository->getBalanceLock($credentials->getID());
                    $newAmount = $balance->getAmount() + $transaction->getAmount();

                    // update balance
                    $balance = $balance->update([
                        'amount' => $newAmount
                    ]);
                }

                // update balance history
                if (in_array($status, BalanceHistoryInterface::STATUS_LIST)) {
                    // get balance history
                    $balanceHistory = $this->balanceHistoryRepository->findByTransaction($id);

                    // save balance history
                    $balanceHistory = $balanceHistory->update([
                        'status' => $status,
                        'description' => $notes,
                    ]);
                }

                // update transaction
                $result = $transaction->update([
                    'status' => $status,
                ]);

                return $transaction;
            });

        } catch (Exception $e) {
            throw new TransactionException($input, $e->getMessage());
        }
    }
}
