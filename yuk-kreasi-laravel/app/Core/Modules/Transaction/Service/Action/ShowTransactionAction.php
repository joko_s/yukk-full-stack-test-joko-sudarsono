<?php

namespace App\Core\Modules\Transaction\Service\Action;

use Exception;
use Illuminate\Database\Eloquent\Collection;

use App\Core\Modules\Transaction\Exception\TransactionException;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;
use App\Core\Modules\Transaction\Service\AbstractTransactionService;
use App\Core\Modules\Transaction\Service\Action\Contract\ShowTransactionActionInterface;

/**
 * Class ShowTransactionAction
 *
 */
class ShowTransactionAction extends AbstractTransactionService implements ShowTransactionActionInterface 
{
    /**
     *
     * @param integer $id
     * @return TransactionInterface|null
     */
    public function handle(int $id): ?TransactionInterface
    {
        $credentials = auth()->user();
        $data = null;

        try {
            $data = $this->transactionRepository->findTransaction($id, $credentials);
        } catch (Exception $e) {
            throw new TransactionException($data, $e->getMessage());
        }

        return $data;
    }
}
