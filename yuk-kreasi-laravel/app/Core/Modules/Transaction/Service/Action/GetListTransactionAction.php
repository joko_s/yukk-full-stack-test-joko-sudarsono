<?php

namespace App\Core\Modules\Transaction\Service\Action;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\Core\Modules\Transaction\Exception\TransactionException;
use App\Core\Modules\Transaction\Service\AbstractTransactionService;
use App\Core\Modules\Transaction\Service\Action\Contract\GetListTransactionActionInterface;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * Class GetListTransactionAction
 *
 */
class GetListTransactionAction extends AbstractTransactionService implements GetListTransactionActionInterface 
{
    /**
     *
     * @param array $input
     * @return LengthAwarePaginator
     */
    public function handle(array $input): LengthAwarePaginator
    {
        $credentials = auth()->user();
        $data = null;

        try {
            $userId = $credentials->isAdmin() ? null : $credentials->getID();
            $data = $this->transactionRepository->getPaginate([
                'user_id' => $userId,
                'bank' => data_get($input, 'bank'),
                'search' => data_get($input, 'search'),
                'account_number_type' => data_get($input, 'account_number_type'),
                'transaction_type' => data_get($input, 'transaction_type'),
                'status' => data_get($input, 'status'),
                'start_date' => data_get($input, 'start_date'),
                'end_date' => data_get($input, 'end_date'),
                'order_by' => data_get($input, 'order_by'),
                'sort' => data_get($input, 'sort'),
                'per_page' => data_get($input, 'per_page'),
                'current_page' => data_get($input, 'current_page'),
            ], $credentials);

        } catch (Exception $e) {
            throw new TransactionException($data, $e->getMessage());
        }

        return $data;
    }
}
