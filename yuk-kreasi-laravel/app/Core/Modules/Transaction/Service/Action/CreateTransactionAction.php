<?php

namespace App\Core\Modules\Transaction\Service\Action;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

use App\Core\Modules\Transaction\Exception\TransactionException;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;
use App\Core\Modules\Transaction\Service\AbstractTransactionService;
use App\Core\Modules\Transaction\Service\Action\Contract\CreateTransactionActionInterface;

/**
 * Class CreateTransactionAction
 *
 */
class CreateTransactionAction extends AbstractTransactionService implements CreateTransactionActionInterface 
{
    /**
     *
     * @param array $input
     * @return array|null
     */
    public function handle(array $input): ?TransactionInterface
    {
        // validate amount
        $transaction = null;
        $credentials = auth()->user();
        $this->validateBalance($input, $credentials);

        try {

            $transaction = DB::transaction(function () use ($input, $credentials) {
                // build payload
                $now = \Carbon\Carbon::now();
                $status = TransactionInterface::STATUS_PENDING;
                $bankID = data_get($input, 'bank_id');
                $accountName = data_get($input, 'account_name');
                $accountNumber = data_get($input, 'account_number');
                $accountNumberType = data_get($input, 'account_number_type');
                $transactionType = data_get($input, 'transaction_type');
                $description = data_get($input, 'description');
                $amount = data_get($input, 'amount');
                $isTransaction = data_get($input, 'transaction_type') == TransactionInterface::TRANSACTION_TYPE_TRANSACTION;

                // create transaction
                $result = $this->transactionRepository->create([
                    'user_id' => $credentials->getID(),
                    'bank_id' => $bankID,
                    'account_name' => $accountName,
                    'account_number' => $accountNumber,
                    'account_number_type' => $accountNumberType,
                    'amount' => $amount,
                    'transaction_type' => $transactionType,
                    'status' => $status,
                    'description' => $description,
                ]);

                // save history
                $history = $this->saveHistory([
                    'transaction_id' => $result->getID(),
                    'status' => $status,
                    'created_at' => $now,
                ]);

                // update balance if transaction type is 'transaction'
                $balance = null;
                $prevBalance = 0;
                if ($isTransaction) {
                    // get balance with lockForUpdate
                    $balance = $this->balanceRepository->getBalanceLock($credentials->getID());
                    $newAmount = $balance->getAmount() - $amount;
                    $prevBalance = $balance->getAmount();

                    // update balance
                    $updateBalance = $balance->update([
                        'amount' => $newAmount
                    ]);
                } else {
                    $balance = $this->balanceRepository->getBalance($credentials->getID());
                    $prevBalance = $balance->getAmount();
                    $newAmount = $balance->getAmount() + $amount;
                }

                // save balance history
                if (!empty($balance)) {
                    $balanceHistory = $this->saveBalanceHistory([
                        'balance_id' => $balance->getID(),
                        'bank_id' => $bankID,
                        'transaction_id' => $result->getID(),
                        'account_name' => $accountName,
                        'account_number' => $accountNumber,
                        'account_number_type' => $accountNumberType,
                        'amount' => $amount,
                        'previous_balance' => $prevBalance,
                        'new_balance' => $newAmount,
                        'transaction_type' => $transactionType,
                        'status' => $status,
                        'description' => $description,
                        'created_at' => $now,
                    ]);
                }

                return $result;
            });

        } catch (Exception $e) {
            throw new TransactionException(null, $e->getMessage());
        }

        return $transaction;
    }
}
