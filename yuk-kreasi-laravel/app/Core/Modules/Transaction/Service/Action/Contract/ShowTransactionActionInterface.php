<?php

namespace App\Core\Modules\Transaction\Service\Action\Contract;

use Illuminate\Database\Eloquent\Collection;

use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;

/**
 * Interface ShowTransactionActionInterface
 *
 */
interface ShowTransactionActionInterface
{
    /**
     *
     * @param integer $id
     * @return TransactionInterface|null
     */
    public function handle(int $id): ?TransactionInterface;
}
