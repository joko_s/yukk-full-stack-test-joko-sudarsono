<?php

namespace App\Core\Modules\Transaction\Service\Action\Contract;

use Illuminate\Database\Eloquent\Collection;

use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;

/**
 * Interface UpdateTransactionActionInterface
 *
 */
interface UpdateTransactionActionInterface
{
    /**
     *
     * @param array $input
     * @return array
     */
    public function handle(array $input): ?TransactionInterface;
}
