<?php

namespace App\Core\Modules\Transaction\Service\Action\Contract;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface GetListTransactionActionInterface
 *
 */
interface GetListTransactionActionInterface
{
    /**
     *
     * @param array $input
     * @return LengthAwarePaginator
     */
    public function handle(array $input): LengthAwarePaginator;
}
