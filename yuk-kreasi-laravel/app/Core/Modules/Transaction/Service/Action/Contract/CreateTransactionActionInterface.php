<?php

namespace App\Core\Modules\Transaction\Service\Action\Contract;

use Illuminate\Database\Eloquent\Collection;

use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;

/**
 * Interface CreateTransactionActionInterface
 *
 */
interface CreateTransactionActionInterface
{
    /**
     *
     * @param array $input
     * @return array
     */
    public function handle(array $input): ?TransactionInterface;
}
