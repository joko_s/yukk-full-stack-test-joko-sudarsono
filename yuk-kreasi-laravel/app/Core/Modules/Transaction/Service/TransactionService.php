<?php

namespace App\Core\Modules\Transaction\Service;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;
use App\Core\Modules\Transaction\Service\AbstractTransactionService;
use App\Core\Modules\Transaction\Service\TransactionServiceInterface;
use App\Core\Modules\Transaction\Service\Action\Contract\CreateTransactionActionInterface;
use App\Core\Modules\Transaction\Service\Action\Contract\GetListTransactionActionInterface;
use App\Core\Modules\Transaction\Service\Action\Contract\ShowTransactionActionInterface;
use App\Core\Modules\Transaction\Service\Action\Contract\UpdateTransactionActionInterface;

/**
 * Class TransactionService
 *
 */
class TransactionService extends AbstractTransactionService implements TransactionServiceInterface 
{
    /**
     *
     * @param array $input
     * @return LengthAwarePaginator
     */
    public function getList(array $input): LengthAwarePaginator
    {
        return app(GetListTransactionActionInterface::class)->handle($input);
    }

    /**
     *
     * @param integer $id
     * @return TransactionInterface|null
     */
    public function show(int $id): ?TransactionInterface
    {
        return app(ShowTransactionActionInterface::class)->handle($id);
    }

    /**
     *
     * @param array $input
     * @return TransactionInterface|null
     */
    public function create(array $input): ?TransactionInterface
    {
        return app(CreateTransactionActionInterface::class)->handle($input);
    }

    /**
     *
     * @param array $input
     * @return TransactionInterface|null
     */
    public function update(array $input): ?TransactionInterface
    {
        return app(UpdateTransactionActionInterface::class)->handle($input);
    }
}
