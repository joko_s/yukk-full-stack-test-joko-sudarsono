<?php

namespace App\Core\Modules\Transaction\Service;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;

/**
 * Interface TransactionServiceInterface
 * 
 */
interface TransactionServiceInterface
{
    /**
     *
     * @param array $input
     * @return LengthAwarePaginator
     */
    public function getList(array $input): LengthAwarePaginator;

    /**
     *
     * @param integer $id
     * @return TransactionInterface|null
     */
    public function show(int $id): ?TransactionInterface;

    /**
     *
     * @param array $input
     * @return TransactionInterface|null
     */
    public function create(array $input): ?TransactionInterface;

    /**
     *
     * @param array $input
     * @return TransactionInterface|null
     */
    public function update(array $input): ?TransactionInterface;
}
