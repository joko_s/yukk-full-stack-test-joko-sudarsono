<?php

namespace App\Core\Modules\Transaction\Service;

use Exception;
use App\Core\Modules\Transaction\Exception\ValidateBalanceException;
use App\Core\Modules\Transaction\Exception\ValidateTransactionException;
use App\Core\Modules\Transaction\Exception\TransactionException;
use App\Core\Modules\Transaction\Entity\Repository\Contract\TransactionRepositoryInterface;
use App\Core\Modules\Transaction\Entity\Repository\Contract\TransactionHistoryRepositoryInterface;
use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceRepositoryInterface;
use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceHistoryRepositoryInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;
use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * Class AbstractTransactionService
 * 
 */
abstract class AbstractTransactionService
{
    /**
     * @var TransactionRepositoryInterface
     */
    protected $transactionRepository;

    /**
     * @var TransactionHistoryRepositoryInterface
     */
    protected $transactionHistoryRepository;

    /**
     * @var BalanceRepositoryInterface
     */
    protected $balanceRepository;

    /**
     * @var BalanceHistoryRepositoryInterface
     */
    protected $balanceHistoryRepository;

    /**
     *
     * @param TransactionRepositoryInterface $transactionRepository
     * @param TransactionHistoryRepositoryInterface $transactionHistoryRepository
     * @param BalanceRepositoryInterface $balanceRepository
     * @param BalanceHistoryRepositoryInterface $balanceHistoryRepository
     */
    public function __construct(
        TransactionRepositoryInterface $transactionRepository,
        TransactionHistoryRepositoryInterface $transactionHistoryRepository,
        BalanceRepositoryInterface $balanceRepository,
        BalanceHistoryRepositoryInterface $balanceHistoryRepository
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->transactionHistoryRepository = $transactionHistoryRepository;
        $this->balanceRepository = $balanceRepository;
        $this->balanceHistoryRepository = $balanceHistoryRepository;
    }

    /**
     *
     * @param [type] $transactionID
     * @return boolean
     */
    protected function validateTransaction($transactionID, $status): bool
    {
        $transaction = $this->transactionRepository->find($transactionID);
        if (empty($transaction)) {
            throw new ValidateTransactionException(null, 'Invalid Transaction Data');
        }

        if ($transaction->getStatus() == TransactionInterface::STATUS_SETTLED) {
            throw new ValidateTransactionException(null, 'Updated failed! Transaction has been settled');
        }

        if ($transaction->getStatus() == TransactionInterface::STATUS_REJECTED) {
            throw new ValidateTransactionException(null, 'Updated failed! Transaction has been rejected');
        }

        $histories = $transaction->getHistories();
        $savedStatus = [];
        foreach ($histories as $key => $history) {
            $savedStatus[] = $history->getStatus();
        }

        if (in_array($status, $savedStatus)) {
            throw new ValidateTransactionException(null, 'Updated failed! Duplicated update request');
        }

        return true;
    }

    /**
     *
     * @param array $input
     * @param UserInterface $credentials
     * @return boolean
     */
    protected function validateBalance(array $input, UserInterface $credentials): bool
    {
        $amount = data_get($input, 'amount');
        $balance = $this->balanceRepository->getBalance($credentials->getID());
        $isTopup = data_get($input, 'transaction_type') == TransactionInterface::TRANSACTION_TYPE_TOPUP;

        // create if not exists
        if (empty($balance)) {
            try {
                $balance = $this->balanceRepository->create([
                    'user_id' => $credentials->getID(),
                    'amount' => 0,
                ]);
            } catch (Exception $e) {
                throw new TransactionException($input, $e->getMessage());
            }
        }

        if (! $isTopup && $balance->getAmount() < $amount) {
            throw new ValidateBalanceException($amount, 'Insufficient Balance');
        }

        return true;
    }

    /**
     *
     * @param array $input
     */
    protected function saveHistory(array $input)
    {
        return $this->transactionHistoryRepository->create($input);
    }

    /**
     *
     * @param array $input
     */
    protected function saveBalanceHistory(array $input)
    {
        return $this->balanceHistoryRepository->create($input);
    }
}
