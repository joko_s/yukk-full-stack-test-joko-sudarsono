<?php

namespace App\Core\Modules\Transaction\Response;

use League\Fractal\TransformerAbstract;

use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;

/**
 * Class GetListTransactionResponse
 * 
 */
class GetListTransactionResponse extends TransformerAbstract
{   
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected array $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected array $availableIncludes = [
        //
    ];

    /**
     * @param array $item
     * @return array
     */
    public function transform(?TransactionInterface $item): ?array
    {
        if (empty($item)) return [];

        $bank = $item->getBank();
        return [
            'id' => $item->getID(),
            'bank' => !empty($bank) ? [
                'id' => $bank->getID(),
                'name' => $bank->getName(),
                'bank_code' => $bank->getBankCode(),
            ] : null,
            'user' => $item->getUser(),
            'account_name' => $item->getAccountName(),
            'account_number' => $item->getAccountNumber(),
            'account_number_type' => $item->getAccountNumberType(),
            'amount' => $item->getAmount(),
            'transaction_type' => $item->getTransactionType(),
            'status' => $item->getStatus(),
            'description' => $item->getDescription(),
            'created_at' => $item->getCreatedAt(),
            'updated_at' => $item->getUpdatedAt(),
        ];
    }
}
