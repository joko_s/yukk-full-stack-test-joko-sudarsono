<?php

namespace App\Core\Modules\Transaction\Entity\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionHistoryInterface;

class TransactionHistory extends Model implements TransactionHistoryInterface
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'transaction_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'transaction_id',
        'status',
        'notes',
        'created_at',
    ];

    /**
     *
     * @var boolean
     */
    public $timestamps = false;

    # ###########################################
    # ################ RELATION #################
    # ###########################################

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################
    /**
     *
     * @return integer
     */
    public function getID(): int
    {
        return $this->id;
    }

    /**
     *
     * @return integer
     */
    public function getTransactionID(): int
    {
        return $this->transaction_id;
    }

    /**
     *
     * @return integer
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     *
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes ?? null;
    }

    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->created_at ? Carbon::parse($this->created_at) : null;
    }
}
