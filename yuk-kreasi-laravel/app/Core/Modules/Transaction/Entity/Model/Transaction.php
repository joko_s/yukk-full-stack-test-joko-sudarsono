<?php

namespace App\Core\Modules\Transaction\Entity\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Core\Foundation\Entity\Model\User;
use App\Core\Modules\Bank\Entity\Model\Bank;
use App\Core\Modules\Transaction\Entity\Model\TransactionHistory;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use App\Core\Modules\Bank\Entity\Model\Contract\BankInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionHistoryInterface;

class Transaction extends Model implements TransactionInterface
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'bank_id',
        'account_name',
        'account_number',
        'account_number_type',
        'amount',
        'transaction_type',
        'status',
        'description',
    ];

    # ###########################################
    # ################ RELATION #################
    # ###########################################
    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'id');
    }

    /**
     *
     * @return HasMany
     */
    public function histories(): HasMany
    {
        return $this->hasMany(TransactionHistory::class, 'transaction_id');
    }

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface
    {
        return $this->user ?? null;
    }

    /**
     *
     * @return BankInterface|null
     */
    public function getBank(): ?BankInterface
    {
        return $this->bank ?? null;
    }

    /**
     *
     * @return Collection|null
     */
    public function getHistories(): ?Collection
    {
        return $this->histories ?? null;
    }

    /**
     *
     * @return integer
     */
    public function getID(): int
    {
        return $this->id;
    }

    /**
     *
     * @return integer
     */
    public function getUserID(): int
    {
        return $this->user_id;
    }

    /**
     *
     * @return integer
     */
    public function getBankID(): int
    {
        return $this->bank_id;
    }

    /**
     *
     * @return string
     */
    public function getAccountName(): string
    {
        return $this->account_name;
    }

    /**
     *
     * @return string
     */
    public function getAccountNumber(): string
    {
        return $this->account_number;
    }

    /**
     *
     * @return integer
     */
    public function getAccountNumberType(): int
    {
        return $this->account_number_type;
    }

    /**
     *
     * @return integer
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     *
     * @return integer
     */
    public function getTransactionType(): int
    {
        return $this->transaction_type;
    }

    /**
     *
     * @return integer
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description ?? null;
    }


    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->created_at ?? null;
    }

    /**
     *
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at ?? null;
    }
}
