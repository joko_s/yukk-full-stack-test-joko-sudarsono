<?php

namespace App\Core\Modules\Transaction\Entity\Model\Contract;

use Carbon\Carbon;
use App\Core\Foundation\Entity\Model\Contract\BaseTransactionInterface;

/**
 * Interface TransactionHistoryInterface
 *
 */
interface TransactionHistoryInterface extends BaseTransactionInterface
{
    # ###########################################
    # ################ RELATION #################
    # ###########################################

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return integer
     */
    public function getID(): int;

    /**
     *
     * @return integer
     */
    public function getTransactionID(): int;

    /**
     *
     * @return integer
     */
    public function getStatus(): int;

    /**
     *
     * @return string|null
     */
    public function getNotes(): ?string;

    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon;
}