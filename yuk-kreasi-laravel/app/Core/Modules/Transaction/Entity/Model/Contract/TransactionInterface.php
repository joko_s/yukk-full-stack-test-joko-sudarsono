<?php

namespace App\Core\Modules\Transaction\Entity\Model\Contract;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

use App\Core\Foundation\Entity\Model\Contract\BaseTransactionInterface;
use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use App\Core\Modules\Bank\Entity\Model\Contract\BankInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionHistoryInterface;

/**
 * Interface TransactionInterface
 *
 */
interface TransactionInterface extends BaseTransactionInterface
{
    # ###########################################
    # ################ RELATION #################
    # ###########################################

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface;

    /**
     *
     * @return BankInterface|null
     */
    public function getBank(): ?BankInterface;

    /**
     *
     * @return Collection|null
     */
    public function getHistories(): ?Collection;

    /**
     *
     * @return integer
     */
    public function getID(): int;

    /**
     *
     * @return integer
     */
    public function getUserID(): int;

    /**
     *
     * @return integer
     */
    public function getBankID(): int;

    /**
     *
     * @return string
     */
    public function getAccountName(): string;

    /**
     *
     * @return string
     */
    public function getAccountNumber(): string;

    /**
     *
     * @return integer
     */
    public function getAccountNumberType(): int;

    /**
     *
     * @return integer
     */
    public function getAmount(): int;

    /**
     *
     * @return integer
     */
    public function getTransactionType(): int;

    /**
     *
     * @return integer
     */
    public function getStatus(): int;

    /**
     *
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon;

    /**
     *
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon;
}