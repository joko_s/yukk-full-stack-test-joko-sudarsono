<?php

namespace App\Core\Modules\Transaction\Entity\Repository\Contract;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TransactionHistoryRepositoryInterface
 *
 */
interface TransactionHistoryRepositoryInterface extends RepositoryInterface
{
    
}
