<?php

namespace App\Core\Modules\Transaction\Entity\Repository\Contract;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;

/**
 * Interface TransactionRepositoryInterface
 *
 */
interface TransactionRepositoryInterface extends RepositoryInterface
{
    /**
     *
     * @param integer $id
     * @param UserInterface $credentials
     * @return TransactionInterface|null
     */
    public function findTransaction(int $id, UserInterface $credentials): ?TransactionInterface;

    /**
     *
     * @param integer $id
     * @param UserInterface $credentials
     * @return TransactionInterface|null
     */
    public function findTransactionLock(int $id, UserInterface $credentials): ?TransactionInterface;

    /**
     *
     * @param array $filter
     * @param UserInterface $credentials
     * @return LengthAwarePaginator
     */
    public function getPaginate(array $filter, UserInterface $credentials): LengthAwarePaginator;
}
