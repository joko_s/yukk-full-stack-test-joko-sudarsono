<?php

namespace App\Core\Modules\Transaction\Entity\Repository;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Prettus\Repository\Eloquent\BaseRepository;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;
use App\Core\Modules\Transaction\Entity\Repository\Contract\TransactionRepositoryInterface;

/**
 * Class TransactionRepository
 *
 */
class TransactionRepository extends BaseRepository implements TransactionRepositoryInterface
{
    /**
     * @return string
     */
    public function model(): string
    {
        return TransactionInterface::class;
    }

    /**
     *
     * @param integer $id
     * @param UserInterface $credentials
     * @return TransactionInterface|null
     */
    public function findTransaction(int $id, UserInterface $credentials): ?TransactionInterface
    {
        $userId = $credentials->isAdmin() ? null : $credentials->getID();

        return $this
            ->with(['histories' => function ($query) {
                return $query->orderBy('created_at', 'desc');
            }])
            ->where('id', $id)
            ->when(! $credentials->isAdmin(), function($query) use ($credentials) {
                return $query->where('user_id', $credentials->getID());
            })
            ->first();
    }

    /**
     *
     * @param integer $id
     * @param UserInterface $credentials
     * @return TransactionInterface|null
     */
    public function findTransactionLock(int $id, UserInterface $credentials): ?TransactionInterface
    {
        return $this
            ->where('id', $id)
            ->lockForUpdate()
            ->first();
    }

    /**
     *
     * @param array $filter
     * @param UserInterface $credentials
     * @return LengthAwarePaginator
     */
    public function getPaginate(array $filter, UserInterface $credentials): LengthAwarePaginator
    {
        return $this
            ->with(['bank', 'user'])
            ->when(!!$filter['user_id'], function($query) use($filter) {
                return $query->where('user_id', $filter['user_id']);
            })
            ->when(!!$filter['bank'], function($query) use($filter) {
                return $query->where('bank_id', $filter['bank_id']);
            })
            ->when(!!$filter['search'], function($query) use($filter) {
                return $query->where(function ($nestedQuery) use ($filter) {
                    return $nestedQuery->where('account_name', 'LIKE', '%' . $filter['search'] . '%')
                        ->orWhere('account_number', 'LIKE', '%' . $filter['search'] . '%');
                });
            })
            ->when(!!$filter['account_number_type'], function($query) use($filter) {
                return $query->where('account_number_type', $filter['account_number_type']);
            })
            ->when(!!$filter['transaction_type'], function($query) use($filter) {
                return $query->where('transaction_type', $filter['transaction_type']);
            })
            ->when(!!$filter['status'], function($query) use($filter) {
                return $query->where('status', $filter['status']);
            })
            ->when(!empty($filter['start_date']) && !empty($filter['end_date']), function($query) use($filter) {
                 $startDate = Carbon::parse($filter['start_date'])->timestamp;
                 $endDate = Carbon::parse($filter['end_date'])->timestamp;
                 return $query->whereBetween('created_at', [$startDate, $endDate]);
             })
            ->when(!empty($filter['order_by']), function($query) use($filter) {
                $sort = $filter['sort'] ?? 'desc';
                return $query->orderBy($filter['order_by'], $sort);
            })
            ->orderBy('id', 'desc')
            ->paginate(
                $filter['per_page'],
                ['*'],
                'page',
                $filter['current_page']
            );
    }
}
