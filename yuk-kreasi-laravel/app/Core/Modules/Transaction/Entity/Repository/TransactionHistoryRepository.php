<?php

namespace App\Core\Modules\Transaction\Entity\Repository;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Prettus\Repository\Eloquent\BaseRepository;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionHistoryInterface;
use App\Core\Modules\Transaction\Entity\Repository\Contract\TransactionHistoryRepositoryInterface;

/**
 * Class TransactionHistoryRepository
 *
 */
class TransactionHistoryRepository extends BaseRepository implements TransactionHistoryRepositoryInterface
{
    /**
     * @return string
     */
    public function model(): string
    {
        return TransactionHistoryInterface::class;
    }
}
