<?php

namespace App\Core\Modules\Transaction\Request;

use App\Core\Foundation\Http\Request\BaseFormRequest;

/**
 * Class UpdateTransactionRequest
 *
 */
class UpdateTransactionRequest extends BaseFormRequest
{
    /**
     *
     * @var array
     */
    protected array $allowed = [
        'transaction_id',
        'status',
        'notes',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'transaction_id' => ['required', 'int'],
            'status' => ['required', 'int'],
         ];
    }
}
