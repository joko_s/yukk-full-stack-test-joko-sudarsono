<?php

namespace App\Core\Modules\Transaction\Request;

use App\Core\Foundation\Http\Request\BaseFormRequest;

/**
 * Class CreateTransactionRequest
 *
 */
class CreateTransactionRequest extends BaseFormRequest
{
    /**
     *
     * @var array
     */
    protected array $allowed = [
        'bank_id',
        'account_name',
        'account_number',
        'account_number_type',
        'amount',
        'transaction_type',
        'description',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'bank_id' => ['required', 'int'],
            'account_name' => ['required'],
            'account_number' => ['required'],
            'account_number_type' => ['required', 'int'],
            'amount' => ['required', 'int'],
            'transaction_type' => ['required', 'int'],
         ];
    }
}
