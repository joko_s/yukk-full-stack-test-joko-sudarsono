<?php

namespace App\Core\Modules\Transaction\Exception;

use App\Core\Foundation\Exception\BaseException;
use App\Core\Foundation\Exception\DisplayableException;
use App\Core\Foundation\Http\Response\HttpCode;
use Exception;

/**
 * Class TransactionException
 *
 */
class TransactionException extends BaseException
{
    /**
     *
     * @param $payload
     * @param string $message
     * @param Exception|null $previous
     */
    public function __construct($payload, string $message = self::DEFAULT_MESSAGE, Exception $previous = null)
    {
        $this->setContent($payload);

        parent::__construct(
            $message,
            HttpCode::INTERNAL_SERVER,
            $previous
        );
    }
}
