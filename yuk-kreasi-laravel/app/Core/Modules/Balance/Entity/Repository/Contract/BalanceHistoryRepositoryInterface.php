<?php

namespace App\Core\Modules\Balance\Entity\Repository\Contract;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BalanceHistoryRepositoryInterface
 *
 */
interface BalanceHistoryRepositoryInterface extends RepositoryInterface
{
    /**
     *
     * @param array $filter
     * @return LengthAwarePaginator
     */
    public function getPaginate(array $filter): LengthAwarePaginator;
}
