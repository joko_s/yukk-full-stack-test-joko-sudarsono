<?php

namespace App\Core\Modules\Balance\Entity\Repository\Contract;

use Prettus\Repository\Contracts\RepositoryInterface;

use App\Core\Modules\Balance\Entity\Model\Contract\BalanceInterface;

/**
 * Interface BalanceRepositoryInterface
 *
 */
interface BalanceRepositoryInterface extends RepositoryInterface
{
    /**
     *
     * @param integer $userID
     * @return BalanceInterface|null
     */
    public function getBalance(int $userID): ?BalanceInterface;

    /**
     *
     * @param integer $userID
     * @return BalanceInterface|null
     */
    public function getBalanceLock(int $userID): ?BalanceInterface;
}
