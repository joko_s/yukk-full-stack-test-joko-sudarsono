<?php

namespace App\Core\Modules\Balance\Entity\Repository;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Prettus\Repository\Eloquent\BaseRepository;

use App\Core\Modules\Balance\Entity\Model\Contract\BalanceHistoryInterface;
use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceHistoryRepositoryInterface;

/**
 * Class BalanceHistoryRepository
 *
 */
class BalanceHistoryRepository extends BaseRepository implements BalanceHistoryRepositoryInterface
{
    /**
     * @return string
     */
    public function model(): string
    {
        return BalanceHistoryInterface::class;
    }

    /**
     *
     * @param integer $userID
     * @return BalanceInterface|null
     */
    public function findByTransaction(int $transactionID): ?BalanceHistoryInterface
    {
        return $this->where('transaction_id', $transactionID)->orderBy('id', 'desc')->first();
    }

    /**
     *
     * @param array $filter
     * @return LengthAwarePaginator
     */
    public function getPaginate(array $filter): LengthAwarePaginator
    {
        return $this
            ->with(['bank'])
            ->when(!!$filter['bank'], function($query) use($filter) {
                return $query->where('bank_id', $filter['bank_id']);
            })
            ->when(!!$filter['search'], function($query) use($filter) {
                return $query->where(function ($nestedQuery) use ($filter) {
                    return $nestedQuery->where('account_name', 'LIKE', '%' . $filter['search'] . '%')
                        ->orWhere('account_number', 'LIKE', '%' . $filter['search'] . '%');
                });
            })
            ->when(!!$filter['account_number_type'], function($query) use($filter) {
                return $query->where('account_number_type', $filter['account_number_type']);
            })
            ->when(!!$filter['transaction_type'], function($query) use($filter) {
                return $query->where('transaction_type', $filter['transaction_type']);
            })
            ->when(!!$filter['status'], function($query) use($filter) {
                return $query->where('status', $filter['status']);
            })
            ->when(!empty($filter['start_date']) && !empty($filter['end_date']), function($query) use($filter) {
                 $startDate = Carbon::parse($filter['start_date'])->timestamp;
                 $endDate = Carbon::parse($filter['end_date'])->timestamp;
                 return $query->whereBetween('created_at', [$startDate, $endDate]);
             })
            ->when(!empty($filter['order_by']), function($query) use($filter) {
                $sort = $filter['sort'] ?? 'desc';
                return $query->orderBy($filter['order_by'], $sort);
            })
            ->when(!!$filter['balance_id'], function($query) use($filter) {
                return $query->where('balance_id', $filter['balance_id']);
            })
            ->orderBy('id', 'desc')
            ->paginate(
                $filter['per_page'],
                ['*'],
                'page',
                $filter['current_page']
            );
    }
}
