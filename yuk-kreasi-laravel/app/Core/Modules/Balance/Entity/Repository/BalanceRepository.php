<?php

namespace App\Core\Modules\Balance\Entity\Repository;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Core\Modules\Balance\Entity\Model\Contract\BalanceInterface;
use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceRepositoryInterface;

/**
 * Class BalanceRepository
 *
 */
class BalanceRepository extends BaseRepository implements BalanceRepositoryInterface
{
    /**
     * @return string
     */
    public function model(): string
    {
        return BalanceInterface::class;
    }

    /**
     *
     * @param integer $userID
     * @return BalanceInterface|null
     */
    public function getBalance(int $userID): ?BalanceInterface
    {
        return $this->where('user_id', $userID)->first();
    }

    /**
     *
     * @param integer $userID
     * @return BalanceInterface|null
     */
    public function getBalanceLock(int $userID): ?BalanceInterface
    {
        return $this->where('user_id', $userID)->lockForUpdate()->first();
    }
}
