<?php

namespace App\Core\Modules\Balance\Entity\Model\Contract;

use Carbon\Carbon;

/**
 * Interface BalanceInterface
 *
 */
interface BalanceInterface
{
    # ###########################################
    # ################ RELATION #################
    # ###########################################

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return integer
     */
    public function getID(): int;

    /**
     *
     * @return integer
     */
    public function getUserID(): int;

    /**
     *
     * @return integer
     */
    public function getAmount(): int;
}