<?php

namespace App\Core\Modules\Balance\Entity\Model\Contract;

use Carbon\Carbon;

use App\Core\Foundation\Entity\Model\Contract\BaseTransactionInterface;
use App\Core\Modules\Bank\Entity\Model\Contract\BankInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;

/**
 * Interface BalanceHistoryInterface
 *
 */
interface BalanceHistoryInterface extends BaseTransactionInterface
{
    const STATUS_LIST = [
        self::STATUS_PENDING,
        self::STATUS_SETTLED,
        self::STATUS_REJECTED,
    ];

    const STATUS_OPTIONS = [
        [self::STATUS_PENDING => self::STATUS_PENDING_TITLE],
        [self::STATUS_SETTLED => self::STATUS_SETTLED_TITLE],
        [self::STATUS_REJECTED => self::STATUS_REJECTED_TITLE],
    ];

    # ###########################################
    # ################ RELATION #################
    # ###########################################

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return BankInterface|null
     */
    public function getBank(): ?BankInterface;

    /**
     *
     * @return TransactionInterface|null
     */
    public function getTransaction(): ?TransactionInterface;

    /**
     *
     * @return integer
     */
    public function getID(): int;

    /**
     *
     * @return integer
     */
    public function getBalanceID(): int;

    /**
     *
     * @return integer
     */
    public function getBankID(): int;

    /**
     *
     * @return integer
     */
    public function getTransactionID(): int;

    /**
     *
     * @return string
     */
    public function getAccountName(): string;

    /**
     *
     * @return string
     */
    public function getAccountNumber(): string;

    /**
     *
     * @return integer
     */
    public function getAccountNumberType(): int;

    /**
     *
     * @return integer
     */
    public function getAmount(): int;

    /**
     *
     * @return integer
     */
    public function getPreviousBalance(): int;

    /**
     *
     * @return integer
     */
    public function getNewBalance(): int;

    /**
     *
     * @return integer
     */
    public function getTransactionType(): int;

    /**
     *
     * @return integer
     */
    public function getStatus(): int;

    /**
     *
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon;
}