<?php

namespace App\Core\Modules\Balance\Entity\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Core\Foundation\Entity\Model\User;
use App\Core\Modules\Balance\Entity\Model\BalanceHistory;
use App\Core\Modules\Balance\Entity\Model\Contract\BalanceInterface;

class Balance extends Model implements BalanceInterface
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'balance';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'amount',
    ];

    public $timestamps = false;

    # ###########################################
    # ################ RELATION #################
    # ###########################################
    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     *
     * @return HasMany
     */
    public function histories(): HasMany
    {
        return $this->hasMany(BalanceHistory::class, 'balance_id');
    }

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return integer
     */
    public function getID(): int
    {
        return $this->id;
    }

    /**
     *
     * @return integer
     */
    public function getUserID(): int
    {
        return $this->user_id;
    }

    /**
     *
     * @return integer
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}
