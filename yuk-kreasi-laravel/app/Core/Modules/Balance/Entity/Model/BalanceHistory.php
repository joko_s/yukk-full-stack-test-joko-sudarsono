<?php

namespace App\Core\Modules\Balance\Entity\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Core\Modules\Bank\Entity\Model\Bank;
use App\Core\Modules\Balance\Entity\Model\Contract\BalanceHistoryInterface;
use App\Core\Modules\Bank\Entity\Model\Contract\BankInterface;
use App\Core\Modules\Transaction\Entity\Model\Contract\TransactionInterface;

class BalanceHistory extends Model implements BalanceHistoryInterface
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'balance_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'balance_id',
        'bank_id',
        'transaction_id',
        'account_name',
        'account_number',
        'account_number_type',
        'amount',
        'previous_balance',
        'new_balance',
        'transaction_type',
        'status',
        'description',
        'created_at',
    ];

    # ###########################################
    # ################ RELATION #################
    # ###########################################
    /**
     * @return BelongsTo
     */
    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'id');
    }

    /**
     *
     * @return HasOne
     */
    public function transaction(): HasOne
    {
        return $this->hasOne(BalanceHistory::class, 'transaction_id');
    }

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return BankInterface|null
     */
    public function getBank(): ?BankInterface
    {
        return $this->bank ?? null;
    }

    /**
     *
     * @return TransactionInterface|null
     */
    public function getTransaction(): ?TransactionInterface
    {
        return $this->transaction ?? null;
    }

    /**
     *
     * @return integer
     */
    public function getID(): int
    {
        return $this->id;
    }

    /**
     *
     * @return integer
     */
    public function getBalanceID(): int
    {
        return $this->balance_id;
    }

    /**
     *
     * @return integer
     */
    public function getBankID(): int
    {
        return $this->bank_id;
    }

    /**
     *
     * @return integer
     */
    public function getTransactionID(): int
    {
        return $this->transaction_id;
    }

    /**
     *
     * @return string
     */
    public function getAccountName(): string
    {
        return $this->account_name;
    }

    /**
     *
     * @return string
     */
    public function getAccountNumber(): string
    {
        return $this->account_number;
    }

    /**
     *
     * @return integer
     */
    public function getAccountNumberType(): int
    {
        return $this->account_number_type;
    }

    /**
     *
     * @return integer
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     *
     * @return integer
     */
    public function getPreviousBalance(): int
    {
        return $this->previous_balance;
    }

    /**
     *
     * @return integer
     */
    public function getNewBalance(): int
    {
        return $this->new_balance;
    }

    /**
     *
     * @return integer
     */
    public function getTransactionType(): int
    {
        return $this->transaction_type;
    }

    /**
     *
     * @return integer
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description ?? null;
    }

    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->created_at ? Carbon::parse($this->created_at) : null;
    }
}
