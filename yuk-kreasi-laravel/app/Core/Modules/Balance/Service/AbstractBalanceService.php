<?php

namespace App\Core\Modules\Balance\Service;

use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceRepositoryInterface;
use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceHistoryRepositoryInterface;

/**
 * Class AbstractBalanceService
 * 
 */
abstract class AbstractBalanceService
{
    /**
     * @var BalanceRepositoryInterface
     */
    protected $balanceRepository;

    /**
     * @var BalanceHistoryRepositoryInterface
     */
    protected $balanceHistoryRepository;

    /**
     *
     * @param BalanceRepositoryInterface $balanceRepository
     * @param BalanceHistoryRepositoryInterface $balanceHistoryRepository
     */
    public function __construct(
        BalanceRepositoryInterface $balanceRepository,
        BalanceHistoryRepositoryInterface $balanceHistoryRepository
    ) {
        $this->balanceRepository = $balanceRepository;
        $this->balanceHistoryRepository = $balanceHistoryRepository;
    }
}
