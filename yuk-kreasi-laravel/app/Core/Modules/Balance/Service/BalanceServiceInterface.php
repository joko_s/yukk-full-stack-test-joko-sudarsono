<?php

namespace App\Core\Modules\Balance\Service;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\Core\Modules\Balance\Entity\Model\Contract\BalanceInterface;

/**
 * Interface BalanceServiceInterface
 * 
 */
interface BalanceServiceInterface
{
    /**
     *
     * @param array $input
     * @return BalanceInterface|null
     */
    public function balance(array $input): ?BalanceInterface;

    /**
     *
     * @param array $input
     * @return Collection|null
     */
    public function histories(array $input): LengthAwarePaginator;
}
