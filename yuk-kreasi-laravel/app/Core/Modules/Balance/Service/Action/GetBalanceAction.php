<?php

namespace App\Core\Modules\Balance\Service\Action;

use Exception;
use Illuminate\Database\Eloquent\Collection;

use App\Core\Modules\Balance\Service\AbstractBalanceService;
use App\Core\Modules\Balance\Service\Action\Contract\GetBalanceActionInterface;
use App\Core\Modules\Balance\Exception\BalanceException;
use App\Core\Modules\Balance\Entity\Model\Contract\BalanceInterface;

/**
 * Class GetBalanceAction
 *
 */
class GetBalanceAction extends AbstractBalanceService implements GetBalanceActionInterface 
{
    /**
     *
     * @param array $input
     * @return BalanceInterface|null
     */
    public function handle(array $input): ?BalanceInterface
    {
        $credentials = auth()->user();
        $data = null;

        try {
            $data = $this->balanceRepository->getBalance($credentials->getID());
        } catch (Exception $e) {
            throw new BalanceException($data, $e->getMessage());
        }

        return $data;
    }
}
