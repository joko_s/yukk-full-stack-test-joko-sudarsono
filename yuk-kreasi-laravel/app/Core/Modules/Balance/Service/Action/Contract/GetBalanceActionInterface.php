<?php

namespace App\Core\Modules\Balance\Service\Action\Contract;

use Illuminate\Database\Eloquent\Collection;

/**
 * Interface GetBalanceActionInterface
 *
 */
interface GetBalanceActionInterface
{
    /**
     *
     * @param array $input
     * @return array
     */
    public function handle(array $input): ?object;
}
