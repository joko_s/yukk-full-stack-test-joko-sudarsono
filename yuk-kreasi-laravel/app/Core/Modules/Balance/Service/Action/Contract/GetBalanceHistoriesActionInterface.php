<?php

namespace App\Core\Modules\Balance\Service\Action\Contract;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface GetBalanceHistoriesActionInterface
 *
 */
interface GetBalanceHistoriesActionInterface
{
    /**
     *
     * @param array $input
     * @return array
     */
    public function handle(array $input): LengthAwarePaginator;
}
