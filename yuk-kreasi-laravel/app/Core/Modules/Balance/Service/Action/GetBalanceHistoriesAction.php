<?php

namespace App\Core\Modules\Balance\Service\Action;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\Core\Modules\Balance\Service\AbstractBalanceService;
use App\Core\Modules\Balance\Service\Action\Contract\GetBalanceHistoriesActionInterface;
use App\Core\Modules\Balance\Exception\BalanceException;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * Class GetBalanceHistoriesAction
 *
 */
class GetBalanceHistoriesAction extends AbstractBalanceService implements GetBalanceHistoriesActionInterface 
{
    /**
     *
     * @param array $input
     * @return array|null
     */
    public function handle(array $input): LengthAwarePaginator
    {
        $credentials = auth()->user();
        $data = null;

        try {
            $balance = $this->balanceRepository->where('user_id', $credentials->getID())->first();
            $balanceID = $credentials->isAdmin() ? null : $balance->getID();

            $data = $this->balanceHistoryRepository->getPaginate([
                'balance_id' => $balanceID,
                'bank' => data_get($input, 'bank'),
                'search' => data_get($input, 'search'),
                'account_number_type' => data_get($input, 'account_number_type'),
                'transaction_type' => data_get($input, 'transaction_type'),
                'status' => data_get($input, 'status'),
                'start_date' => data_get($input, 'start_date'),
                'end_date' => data_get($input, 'end_date'),
                'order_by' => data_get($input, 'order_by'),
                'sort' => data_get($input, 'sort'),
                'per_page' => data_get($input, 'per_page'),
                'current_page' => data_get($input, 'current_page'),
            ]);

        } catch (Exception $e) {
            throw new BalanceException($data, $e->getMessage());
        }

        return $data;
    }
}
