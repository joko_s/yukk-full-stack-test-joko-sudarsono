<?php

namespace App\Core\Modules\Balance\Service;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\Core\Modules\Balance\Service\AbstractBalanceService;
use App\Core\Modules\Balance\Service\BalanceServiceInterface;
use App\Core\Modules\Balance\Service\Action\Contract\GetBalanceActionInterface;
use App\Core\Modules\Balance\Service\Action\Contract\GetBalanceHistoriesActionInterface;
use App\Core\Modules\Balance\Entity\Model\Contract\BalanceInterface;

/**
 * Class BalanceService
 *
 */
class BalanceService extends AbstractBalanceService implements BalanceServiceInterface 
{
    /**
     *
     * @param array $input
     * @return BalanceInterface|null
     */
    public function balance(array $input): ?BalanceInterface
    {
        return app(GetBalanceActionInterface::class)->handle($input);
    }

    /**
     *
     * @param array $input
     * @return Collection|null
     */
    public function histories(array $input): LengthAwarePaginator
    {
        return app(GetBalanceHistoriesActionInterface::class)->handle($input);
    }
}
