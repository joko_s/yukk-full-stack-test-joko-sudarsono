<?php

namespace App\Core\Modules\Balance\Response;

use League\Fractal\TransformerAbstract;

use App\Core\Modules\Balance\Entity\Model\Contract\BalanceHistoryInterface;

/**
 * Class GetBalanceHistoriesResponse
 * 
 */
class GetBalanceHistoriesResponse extends TransformerAbstract
{   
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected array $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected array $availableIncludes = [
        //
    ];

    /**
     * @param array $item
     * @return array
     */
    public function transform(?BalanceHistoryInterface $item): ?array
    {
        if (empty($item)) return [];

        $bank = $item->getBank();
        return [
            'id' => $item->getID(),
            'balance_id' => $item->getBalanceID(),
            'bank' => !empty($bank) ? [
                'id' => $bank->getID(),
                'name' => $bank->getName(),
                'bank_code' => $bank->getBankCode(),
            ] : null,
            'account_name' => $item->getAccountName(),
            'account_number' => $item->getAccountNumber(),
            'account_number_type' => $item->getAccountNumberType(),
            'amount' => $item->getAmount(),
            'previous_balance' => $item->getPreviousBalance(),
            'new_balance' => $item->getNewBalance(),
            'transaction_type' => $item->getTransactionType(),
            'status' => $item->getStatus(),
            'description' => $item->getDescription(),
            'created_at' => $item->getCreatedAt(),
        ];
    }
}
