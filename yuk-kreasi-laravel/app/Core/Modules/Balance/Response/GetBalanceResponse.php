<?php

namespace App\Core\Modules\Balance\Response;

use League\Fractal\TransformerAbstract;

use App\Core\Modules\Balance\Entity\Model\Contract\BalanceInterface;

/**
 * Class GetBalanceResponse
 * 
 */
class GetBalanceResponse extends TransformerAbstract
{   
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected array $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected array $availableIncludes = [
        //
    ];

    /**
     * @param array $item
     * @return array
     */
    public function transform(?BalanceInterface $item): ?array
    {
        if (empty($item)) return (object) [];

        return [
            'id' => $item->getID(),
            'user_id' => $item->getUserID(),
            'amount' => $item->getAmount(),
        ];
    }
}
