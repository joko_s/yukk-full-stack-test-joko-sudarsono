<?php

namespace App\Core\Modules\Balance;

use Illuminate\Support\ServiceProvider;

use App\Core\Modules\Balance\Entity\Model\Contract\BalanceInterface;
use App\Core\Modules\Balance\Entity\Model\Contract\BalanceHistoryInterface;
use App\Core\Modules\Balance\Entity\Model\Balance;
use App\Core\Modules\Balance\Entity\Model\BalanceHistory;

use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceRepositoryInterface;
use App\Core\Modules\Balance\Entity\Repository\Contract\BalanceHistoryRepositoryInterface;
use App\Core\Modules\Balance\Entity\Repository\BalanceRepository;
use App\Core\Modules\Balance\Entity\Repository\BalanceHistoryRepository;

use App\Core\Modules\Balance\Service\Action\Contract\GetBalanceActionInterface;
use App\Core\Modules\Balance\Service\Action\Contract\GetBalanceHistoriesActionInterface;
use App\Core\Modules\Balance\Service\Action\GetBalanceAction;
use App\Core\Modules\Balance\Service\Action\GetBalanceHistoriesAction;

use App\Core\Modules\Balance\Service\BalanceServiceInterface;
use App\Core\Modules\Balance\Service\BalanceService;

/**
 * Class BalanceProvider
 *
 */
class BalanceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {
        // entity
        $this->app->bind(BalanceInterface::class, Balance::class);
        $this->app->bind(BalanceHistoryInterface::class, BalanceHistory::class);

        // repository
        $this->app->bind(BalanceRepositoryInterface::class, BalanceRepository::class);
        $this->app->bind(BalanceHistoryRepositoryInterface::class, BalanceHistoryRepository::class);

        // Action
        $this->app->bind(GetBalanceActionInterface::class, GetBalanceAction::class);
        $this->app->bind(GetBalanceHistoriesActionInterface::class, GetBalanceHistoriesAction::class);

        // Service
        $this->app->bind(BalanceServiceInterface::class, BalanceService::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            // entity
            Balance::class,
            BalanceHistory::class,

            // repository
            BalanceRepository::class,
            BalanceHistoryRepository::class,

            // Action
            GetBalanceAction::class,
            GetBalanceHistoriesAction::class,

            // Service
            BalanceService::class,
        ];
    }
}
