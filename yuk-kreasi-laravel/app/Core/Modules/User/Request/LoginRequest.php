<?php

namespace App\Core\Modules\User\Request;

use App\Core\Foundation\Http\Request\BaseFormRequest;

/**
 * Class LoginRequest
 *
 * @package App\Core\Modules\User\Request
 */
class LoginRequest extends BaseFormRequest
{
    /**
     *
     * @var array
     */
    protected array $allowed = [
        'email',
        'password',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'     => ['required', 'email:rfc,dns'],
            'password' => ['required', 'string'],
         ];
    }
}
