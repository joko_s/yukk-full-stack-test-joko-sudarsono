<?php

namespace App\Core\Modules\User\Request;

use App\Core\Foundation\Http\Request\BaseFormRequest;

/**
 * Class RegisterRequest
 *
 * @package App\Core\Modules\User\Request
 */
class RegisterRequest extends BaseFormRequest
{
    /**
     *
     * @var array
     */
    protected array $allowed = [
        'name',
        'email',
        'password',
        'phone',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email:rfc,dns', 'unique:users'],
            'password' => ['required', 'string'],
         ];
    }
}
