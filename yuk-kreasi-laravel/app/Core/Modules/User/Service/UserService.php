<?php

namespace App\Core\Modules\User\Service;

use Exception;

use App\Core\Modules\User\Service\AbstractUserService;
use App\Core\Modules\User\Service\UserServiceInterface;
use App\Core\Modules\User\Service\Action\Contract\UserLoginActionInterface;
use App\Core\Modules\User\Service\Action\Contract\UserRegisterActionInterface;
use App\Core\Modules\User\Service\Action\Contract\UserLogoutActionInterface;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * Class UserService
 *
 * @package AApp\Core\Modules\User\Service
 */
class UserService extends AbstractUserService implements UserServiceInterface 
{
    /**
     * @inheritDoc
     */
    public function login(array $input): ?object
    {
        return app(UserLoginActionInterface::class)->handle($input);
    }

    /**
     * @inheritDoc
     */
    public function register(array $input): ?UserInterface
    {
        return app(UserRegisterActionInterface::class)->handle($input);
    }

    /**
     *
     * @param array $input
     * @return array|null
     */
    public function logout(array $input): ?object
    {
        return app(UserLogoutActionInterface::class)->handle($input);
    }
}
