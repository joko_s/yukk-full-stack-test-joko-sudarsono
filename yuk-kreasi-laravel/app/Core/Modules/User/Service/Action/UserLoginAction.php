<?php

namespace App\Core\Modules\User\Service\Action;

use Exception;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

use App\Core\Modules\User\Service\AbstractUserService;
use App\Core\Modules\User\Service\Action\Contract\UserLoginActionInterface;
use App\Core\Modules\User\Exception\UserLoginException;

/**
 * Class UserLoginAction
 *
 * @package App\Core\Modules\User\Service\Action
 */
class UserLoginAction extends AbstractUserService implements UserLoginActionInterface 
{
    /**
     *
     * @param array $input
     * @return array|null
     */
    public function handle(array $input): ?object
    {
        $token = auth()->attempt($input);;
        if (!$token) {
            throw new UserLoginException(data_get($input, 'email'), 'Invalid Username or Password');
        }

        $user = auth()->user();
        $data = [
            'user'  => $user,
            'token' => $token
        ];

        return (object) $data;
    }
}
