<?php

namespace App\Core\Modules\User\Service\Action;

use Exception;
use Illuminate\Support\Facades\DB;

use App\Core\Modules\User\Service\AbstractUserService;
use App\Core\Modules\User\Service\Action\Contract\UserRegisterActionInterface;
use App\Core\Modules\User\Exception\UserRegisterException;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * Class UserRegisterAction
 *
 * @package App\Core\Modules\User\Service\Action
 */
class UserRegisterAction extends AbstractUserService implements UserRegisterActionInterface 
{
    /**
     *
     * @param array $input
     * @return UserInterface|null
     */
    public function handle(array $input): ?UserInterface
    {
        $result = null;

        try {
            $result = $this->userRepository->create(array_merge($input, [
                'user_role' => UserInterface::ROLE_USER,
            ]));
        } catch (Exception $e) {
            throw new UserRegisterException($result, $e->getMessage());
        }

        return $result;
    }
}
