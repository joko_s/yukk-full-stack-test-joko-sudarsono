<?php

namespace App\Core\Modules\User\Service\Action;

use Exception;

use App\Core\Modules\User\Service\AbstractUserService;
use App\Core\Modules\User\Service\Action\Contract\UserLogoutActionInterface;

/**
 * Class UserLogoutAction
 *
 * @package App\Core\Modules\User\Service\Action
 */
class UserLogoutAction extends AbstractUserService implements UserLogoutActionInterface 
{
    /**
     *
     * @param array $input
     * @return array|null
     */
    public function handle(array $input): ?object
    {
        auth()->logout();
        return null;
    }
}
