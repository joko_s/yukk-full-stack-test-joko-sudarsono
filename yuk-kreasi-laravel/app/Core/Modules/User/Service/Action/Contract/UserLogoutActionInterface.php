<?php

namespace App\Core\Modules\User\Service\Action\Contract;

/**
 * Interface UserLogoutActionInterface
 *
 * @package App\Core\Modules\User\Service\Action\Contract;
 */
interface UserLogoutActionInterface
{
    /**
     *
     * @param array $input
     * @return array
     */
    public function handle(array $input): ?object;
}
