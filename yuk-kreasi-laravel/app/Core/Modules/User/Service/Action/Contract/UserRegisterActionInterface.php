<?php

namespace App\Core\Modules\User\Service\Action\Contract;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * Interface UserRegisterActionInterface
 *
 * @package App\Core\Modules\User\Service\Action\Contract;
 */
interface UserRegisterActionInterface
{
    /**
     *
     * @param array $input
     * @return UserInterface|null
     */
    public function handle(array $input): ?UserInterface;
}
