<?php

namespace App\Core\Modules\User\Service\Action\Contract;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * Interface UserLoginActionInterface
 *
 * @package App\Core\Modules\User\Service\Action\Contract;
 */
interface UserLoginActionInterface
{
    /**
     *
     * @param array $input
     * @return array
     */
    public function handle(array $input): ?object;
}
