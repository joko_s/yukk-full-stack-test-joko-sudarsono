<?php

namespace App\Core\Modules\User\Service;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * Interface UserServiceInterface
 * 
 * @package App\Core\Modules\User\Service
 */
interface UserServiceInterface
{
    /**
     *
     * @param array $input
     * @return array|null
     */
    public function login(array $input): ?object;

    /**
     *
     * @param array $input
     * @return UserInterface|null
     */
    public function register(array $input): ?UserInterface;

    /**
     *
     * @param array $input
     * @return array|null
     */
    public function logout(array $input): ?object;
}
