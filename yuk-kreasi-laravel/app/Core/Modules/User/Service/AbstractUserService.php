<?php

namespace App\Core\Modules\User\Service;

use App\Core\Modules\User\Entity\Repository\Contract\UserRepositoryInterface;

/**
 * Class AbstractUserService
 * 
 * @package App\Core\Modules\User\Service
 */
abstract class AbstractUserService
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
}
