<?php

namespace App\Core\Modules\User\Exception;

use App\Core\Foundation\Exception\DisplayableException;
use App\Core\Foundation\Http\Response\HttpCode;
use Exception;

/**
 * Class UserLoginException
 *
 * @package App\Core\Modules\User\Exception
 */
class UserLoginException extends DisplayableException
{
    /**
     *
     * @param $payload
     * @param string $message
     * @param Exception|null $previous
     */
    public function __construct($payload, string $message = self::DEFAULT_MESSAGE, Exception $previous = null)
    {
        $this->setContent($payload);

        parent::__construct(
            $message,
            HttpCode::UNPROCESSABLE_ENTITY,
            $previous
        );
    }
}
