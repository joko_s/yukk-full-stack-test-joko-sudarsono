<?php

namespace App\Core\Modules\User;

use Illuminate\Support\ServiceProvider;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use App\Core\Foundation\Entity\Model\User;

use App\Core\Modules\User\Entity\Repository\Contract\UserRepositoryInterface;
use App\Core\Modules\User\Entity\Repository\UserRepository;

use App\Core\Modules\User\Service\Action\Contract\UserLoginActionInterface;
use App\Core\Modules\User\Service\Action\Contract\UserRegisterActionInterface;
use App\Core\Modules\User\Service\Action\Contract\UserLogoutActionInterface;
use App\Core\Modules\User\Service\Action\UserLoginAction;
use App\Core\Modules\User\Service\Action\UserRegisterAction;
use App\Core\Modules\User\Service\Action\UserLogoutAction;

use App\Core\Modules\User\Service\UserServiceInterface;
use App\Core\Modules\User\Service\UserService;

/**
 * Class UserProvider
 *
 * @package App\Core\Modules\User
 */
class UserProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {
        // entity
        $this->app->bind(UserInterface::class, User::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);

        // Action
        $this->app->bind(UserLoginActionInterface::class, UserLoginAction::class);
        $this->app->bind(UserRegisterActionInterface::class, UserRegisterAction::class);
        $this->app->bind(UserLogoutActionInterface::class, UserLogoutAction::class);

        // Service
        $this->app->bind(UserServiceInterface::class, UserService::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            // entity
            User::class,
            UserRepository::class,

            // Action
            UserLoginAction::class,
            UserRegisterAction::class,
            UserLogoutAction::class,

            // Service
            UserService::class,
        ];
    }
}
