<?php

namespace App\Core\Modules\User\Entity\Repository\Contract;

use Prettus\Repository\Contracts\RepositoryInterface;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * Interface UserRepositoryInterface
 *
 * @package App\Core\Modules\User\Entity\Contract
 */
interface UserRepositoryInterface extends RepositoryInterface
{
    
}
