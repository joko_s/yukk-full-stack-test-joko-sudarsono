<?php

namespace App\Core\Modules\User\Entity\Repository;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use App\Core\Modules\User\Entity\Repository\Contract\UserRepositoryInterface;

/**
 * Class UserRepository
 *
 * @package App\Core\Modules\User\Entity\Repository
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * @return string
     */
    public function model(): string
    {
        return UserInterface::class;
    }
}
