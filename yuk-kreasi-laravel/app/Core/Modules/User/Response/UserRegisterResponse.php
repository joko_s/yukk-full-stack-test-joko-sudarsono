<?php

namespace App\Core\Modules\User\Response;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use League\Fractal\TransformerAbstract;

/**
 * Class UserRegisterResponse
 * 
 * @package App\Core\Modules\User\Response
 */
class UserRegisterResponse extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected array $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected array $availableIncludes = [
        //
    ];

    /**
     * @param UserInterface|null $item
     * @return array
     */
    public function transform(?UserInterface $item): array
    {
        if (empty($item)) return [];

        return [
            'id' => $item->getID(),
            'name' => $item->getName(),
            'email' => $item->getEmail(),
            'phone' => $item->getPhone(),
            'user_role' => $item->getUserRole(),
            'created_at' => $item->getCreatedAt(),
            'updated_at' => $item->getUpdatedAt(),
        ];
    }
}
