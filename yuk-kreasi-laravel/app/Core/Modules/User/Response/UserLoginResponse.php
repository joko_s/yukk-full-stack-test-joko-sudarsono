<?php

namespace App\Core\Modules\User\Response;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use League\Fractal\TransformerAbstract;

/**
 * Class UserLoginResponse
 * 
 * @package App\Core\Modules\User\Response
 */
class UserLoginResponse extends TransformerAbstract
{   
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected array $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected array $availableIncludes = [
        //
    ];

    /**
     * @param array $item
     * @return array
     */
    public function transform(?object $item): ?array
    {
        if (empty($item)) return [
            'user' => (object) [],
            'token' => '',
        ];

        return [
            'user' => $item->user,
            'token' => $item->token,
        ];
    }
}
