<?php

namespace App\Core\Modules\Bank;

use Illuminate\Support\ServiceProvider;

use App\Core\Modules\Bank\Entity\Model\Contract\BankInterface;
use App\Core\Modules\Bank\Entity\Model\Bank;

use App\Core\Modules\Bank\Entity\Repository\Contract\BankRepositoryInterface;
use App\Core\Modules\Bank\Entity\Repository\BankRepository;

use App\Core\Modules\Bank\Service\Action\Contract\GetBankListActionInterface;
use App\Core\Modules\Bank\Service\Action\GetBankListAction;

use App\Core\Modules\Bank\Service\BankServiceInterface;
use App\Core\Modules\Bank\Service\BankService;

/**
 * Class BankProvider
 *
 */
class BankProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {
        // entity
        $this->app->bind(BankInterface::class, Bank::class);
        $this->app->bind(BankRepositoryInterface::class, BankRepository::class);

        // Action
        $this->app->bind(GetBankListActionInterface::class, GetBankListAction::class);

        // Service
        $this->app->bind(BankServiceInterface::class, BankService::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            // entity
            Bank::class,
            BankRepository::class,

            // Action
            GetBankListAction::class,

            // Service
            BankService::class,
        ];
    }
}
