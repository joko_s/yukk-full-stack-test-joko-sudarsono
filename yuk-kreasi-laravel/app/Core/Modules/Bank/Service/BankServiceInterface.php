<?php

namespace App\Core\Modules\Bank\Service;

use Illuminate\Database\Eloquent\Collection;

/**
 * Interface BankServiceInterface
 * 
 */
interface BankServiceInterface
{
    /**
     *
     * @param array $input
     * @return Collection|null
     */
    public function getList(array $input): ?Collection;
}
