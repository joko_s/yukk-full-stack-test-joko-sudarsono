<?php

namespace App\Core\Modules\Bank\Service;

use App\Core\Modules\Bank\Entity\Repository\Contract\BankRepositoryInterface;

/**
 * Class AbstractBankService
 * 
 */
abstract class AbstractBankService
{
    /**
     * @var BankRepositoryInterface
     */
    protected $bankRepository;

    /**
     *
     * @param BankRepositoryInterface $bankRepository
     */
    public function __construct(BankRepositoryInterface $bankRepository)
    {
        $this->bankRepository = $bankRepository;
    }
}
