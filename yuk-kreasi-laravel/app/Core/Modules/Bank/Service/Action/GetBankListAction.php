<?php

namespace App\Core\Modules\Bank\Service\Action;

use Exception;
use Illuminate\Database\Eloquent\Collection;

use App\Core\Modules\Bank\Service\AbstractBankService;
use App\Core\Modules\Bank\Service\Action\Contract\GetBankListActionInterface;
use App\Core\Modules\Bank\Exception\BankException;

/**
 * Class GetBankListAction
 *
 */
class GetBankListAction extends AbstractBankService implements GetBankListActionInterface 
{
    /**
     *
     * @param array $input
     * @return array|null
     */
    public function handle(array $input): ?Collection
    {
        $data = null;
        try {
            $data = $this->bankRepository->orderBy('name')->all();
        } catch (Exception $e) {
            throw new BankException($data, $e->getMessage());
        }

        return $data;
    }
}
