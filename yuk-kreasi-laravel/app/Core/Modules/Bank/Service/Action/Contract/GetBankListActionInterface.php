<?php

namespace App\Core\Modules\Bank\Service\Action\Contract;

use Illuminate\Database\Eloquent\Collection;

/**
 * Interface GetBankListActionInterface
 *
 */
interface GetBankListActionInterface
{
    /**
     *
     * @param array $input
     * @return array
     */
    public function handle(array $input): ?Collection;
}
