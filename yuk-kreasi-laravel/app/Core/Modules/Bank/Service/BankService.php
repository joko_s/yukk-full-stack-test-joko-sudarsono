<?php

namespace App\Core\Modules\Bank\Service;

use Exception;
use Illuminate\Database\Eloquent\Collection;

use App\Core\Modules\Bank\Service\AbstractBankService;
use App\Core\Modules\Bank\Service\BankServiceInterface;
use App\Core\Modules\Bank\Service\Action\Contract\GetBankListActionInterface;

/**
 * Class BankService
 *
 */
class BankService extends AbstractBankService implements BankServiceInterface 
{
    /**
     *
     * @param array $input
     * @return Collection|null
     */
    public function getList(array $input): ?Collection
    {
        return app(GetBankListActionInterface::class)->handle($input);
    }
}
