<?php

namespace App\Core\Modules\Bank\Response;

use League\Fractal\TransformerAbstract;

use App\Core\Modules\Bank\Entity\Model\Contract\BankInterface;

/**
 * Class GetBankListResponse
 * 
 */
class GetBankListResponse extends TransformerAbstract
{   
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected array $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected array $availableIncludes = [
        //
    ];

    /**
     * @param array $item
     * @return array
     */
    public function transform(?BankInterface $item): ?array
    {
        if (empty($item)) return [];

        return [
            'id' => $item->getID(),
            'name' => $item->getName(),
            'bank_code' => $item->getBankCode(),
        ];
    }
}
