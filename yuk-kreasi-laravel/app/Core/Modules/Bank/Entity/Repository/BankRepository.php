<?php

namespace App\Core\Modules\Bank\Entity\Repository;

use Prettus\Repository\Eloquent\BaseRepository;

use App\Core\Modules\Bank\Entity\Model\Contract\BankInterface;
use App\Core\Modules\Bank\Entity\Repository\Contract\BankRepositoryInterface;

/**
 * Class BankRepository
 *
 */
class BankRepository extends BaseRepository implements BankRepositoryInterface
{
    /**
     * @return string
     */
    public function model(): string
    {
        return BankInterface::class;
    }
}
