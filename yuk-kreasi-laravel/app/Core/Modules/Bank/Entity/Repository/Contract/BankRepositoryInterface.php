<?php

namespace App\Core\Modules\Bank\Entity\Repository\Contract;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BankRepositoryInterface
 *
 */
interface BankRepositoryInterface extends RepositoryInterface
{
    
}
