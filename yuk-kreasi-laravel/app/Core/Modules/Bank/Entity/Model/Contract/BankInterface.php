<?php

namespace App\Core\Modules\Bank\Entity\Model\Contract;

use Carbon\Carbon;

/**
 * Interface BankInterface
 *
 */
interface BankInterface
{
    # ###########################################
    # ################ RELATION #################
    # ###########################################

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return integer
     */
    public function getID(): int;
    /**
     *
     * @return string
     */
    public function getName(): string;

    /**
     *
     * @return string
     */
    public function getBankCode(): string;

    /**
     *
     * @return integer|null
     */
    public function getCreatedBy(): ?int;

    /**
     *
     * @return integer|null
     */
    public function getUpdatedBy(): ?int;

    /**
     *
     * @return integer|null
     */
    public function getDeletedBy(): ?int;

    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon;

    /**
     *
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon;
}