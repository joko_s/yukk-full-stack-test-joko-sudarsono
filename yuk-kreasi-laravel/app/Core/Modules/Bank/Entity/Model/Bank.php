<?php

namespace App\Core\Modules\Bank\Entity\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Core\Modules\Bank\Entity\Model\Contract\BankInterface;

class Bank extends Model implements BankInterface
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'bank_code',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    # ###########################################
    # ################ RELATION #################
    # ###########################################

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return integer
     */
    public function getID(): int
    {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @return string
     */
    public function getBankCode(): string
    {
        return $this->bank_code;
    }

    /**
     *
     * @return integer|null
     */
    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    /**
     *
     * @return integer|null
     */
    public function getUpdatedBy(): ?int
    {
        return $this->updated_by;
    }

    /**
     *
     * @return integer|null
     */
    public function getDeletedBy(): ?int
    {
        return $this->deleted_by;
    }

    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->created_at ?? null;
    }

    /**
     *
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at ?? null;
    }
}
