<?php

namespace App\Core\Foundation\Entity\Model\Contract;

use Carbon\Carbon;

/**
 * Interface UserInterface
 *
 */
interface UserInterface
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;

    const ROLE_ADMIN_NAME = 'Admin';
    const ROLE_USER_NAME = 'User';

    const ROLES = [
        self::ROLE_ADMIN,
        self::ROLE_USER,
    ];

    const ROLE_OPTIONS = [
        self::ROLE_ADMIN    => self::ROLE_ADMIN_NAME,
        self::ROLE_USER     => self::ROLE_USER_NAME,
    ];

    # ###########################################
    # ################ RELATION #################
    # ###########################################

    # ###########################################
    # ################# SETTER ##################
    # ###########################################

    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return integer
     */
    public function getID(): int;

    /**
     *
     * @return string
     */
    public function getName(): string;

    /**
     *
     * @return string
     */
    public function getEmail(): string;

    /**
     *
     * @return string|null
     */
    public function getPhone(): ?string;

    /**
     *
     * @return integer
     */
    public function getUserRole(): int;

    /**
     *
     * @return integer|null
     */
    public function getCreatedBy(): ?int;

    /**
     *
     * @return integer|null
     */
    public function getUpdatedBy(): ?int;

    /**
     *
     * @return integer|null
     */
    public function getDeletedBy(): ?int;

    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon;

    /**
     *
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon;


    # ###########################################
    # ############# CUSTOM GETTER ###############
    # ###########################################
    /**
     *
     * @return boolean
     */
    public function isAdmin(): bool;
}
