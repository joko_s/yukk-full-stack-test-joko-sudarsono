<?php

namespace App\Core\Foundation\Entity\Model\Contract;

use Carbon\Carbon;

/**
 * Interface BaseTransactionInterface
 *
 */
interface BaseTransactionInterface
{
    const ACCOUNT_NUMBER_TYPE_BANK = 1;
    const ACCOUNT_NUMBER_TYPE_VA = 2;

    const ACCOUNT_NUMBER_TYPES = [
        self::ACCOUNT_NUMBER_TYPE_BANK,
        self::ACCOUNT_NUMBER_TYPE_VA,
    ];

    const ACCOUNT_NUMBER_TYPE_OPTIONS = [
        [self::ACCOUNT_NUMBER_TYPE_BANK => 'Bank Account'],
        [self::ACCOUNT_NUMBER_TYPE_VA => 'Virtual Account'],
    ];

    const TRANSACTION_TYPE_TOPUP = 1;
    const TRANSACTION_TYPE_TRANSACTION = 2;

    const TRANSACTION_TYPES = [
        self::TRANSACTION_TYPE_TOPUP,
        self::TRANSACTION_TYPE_TRANSACTION,
    ];

    const TRANSACTION_TYPE_OPTIONS = [
        [self::TRANSACTION_TYPE_TOPUP => 'Top Up'],
        [self::TRANSACTION_TYPE_TRANSACTION => 'Transaction'],
    ];

    const STATUS_PENDING = 1;
    const STATUS_PROCESSED = 2;
    const STATUS_SETTLED = 3;
    const STATUS_REJECTED = 4;

    const STATUS_PENDING_TITLE = 'Pending';
    const STATUS_PROCESSED_TITLE = 'Processed';
    const STATUS_SETTLED_TITLE = 'Settled';
    const STATUS_REJECTED_TITLE = 'Rejected';

    const STATUS_LIST = [
        self::STATUS_PENDING,
        self::STATUS_PROCESSED,
        self::STATUS_SETTLED,
        self::STATUS_REJECTED,
    ];

    const STATUS_OPTIONS = [
        [self::STATUS_PENDING => self::STATUS_PENDING_TITLE],
        [self::STATUS_PROCESSED => self::STATUS_PROCESSED_TITLE],
        [self::STATUS_SETTLED => self::STATUS_SETTLED_TITLE],
        [self::STATUS_REJECTED => self::STATUS_REJECTED_TITLE],
    ];
}