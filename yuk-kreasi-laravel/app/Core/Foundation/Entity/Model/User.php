<?php

namespace App\Core\Foundation\Entity\Model;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

use Carbon\Carbon;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;

/**
 * class User
 */
class User extends Authenticatable implements UserInterface, JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'user_role',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];


    # ###########################################
    # ################# GETTER ##################
    # ###########################################

    /**
     *
     * @return integer
     */
    public function getID(): int
    {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     *
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone ?? null;
    }

    /**
     *
     * @return integer
     */
    public function getUserRole(): int
    {
        return $this->user_role;
    }

    /**
     *
     * @return integer|null
     */
    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    /**
     *
     * @return integer|null
     */
    public function getUpdatedBy(): ?int
    {
        return $this->updated_by;
    }

    /**
     *
     * @return integer|null
     */
    public function getDeletedBy(): ?int
    {
        return $this->deleted_by;
    }

    /**
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->created_at ?? null;
    }

    /**
     *
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at ?? null;
    }


    # ###########################################
    # ############# CUSTOM GETTER ###############
    # ###########################################
    /**
     *
     * @return boolean
     */
    public function isAdmin(): bool
    {
        return $this->getUserRole() == self::ROLE_ADMIN;
    }


    # ###########################################
    # ################# JWT CONFIG ##################
    # ###########################################

     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
