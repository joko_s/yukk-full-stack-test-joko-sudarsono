<?php

namespace App\Core\Foundation\Exception;

/**
 * Class DisplayableException
 *
 * @package App\Core\Foundation\Exception
 */
class DisplayableException extends BaseException
{

}
