<?php

namespace App\Core\Foundation\Exception;

use App\Exceptions\Handler;

/**
 * Class BaseException
 *
 * @package App\Core\Foundation\Exception
 */
class BaseException extends \Exception
{
    const DEFAULT_MESSAGE = 'Exceptions Error';

    protected $contents = [];

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    protected function setContent($content)
    {
        if (null === $content || is_scalar($content) || is_array($content)) {
            $this->contents[] = $content;
        } elseif (method_exists($content, 'toArray')) {
            $this->contents[] = $content->toArray();
        }
    }

    public function getContents()
    {
        return $this->contents;
    }
}
