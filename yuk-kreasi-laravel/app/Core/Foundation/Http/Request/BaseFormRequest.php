<?php

namespace App\Core\Foundation\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

use App\Core\Foundation\Http\Response\HttpCode;

/**
 * Class BaseFormRequest
 *
 * @package App\Core\Modules\User\Request
 */
class BaseFormRequest extends FormRequest
{
    /**
     *
     * @var array
     */
    protected array $allowed = [];

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        // remove unused input field
        if (is_array($this->allowed) && count($this->allowed) > 0) {
            $replacements = [];
            foreach ($this->all() as $key => $input) {
                if (in_array($key, $this->allowed)) {
                    $replacements[$key] = $input;
                }
            }

            $this->replace($replacements);
        }
    }

    /**
     * Get the error messages for the defined validation rules
     * 
     * @return array
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'errors'    => $validator->errors(),
            'status'    => false,
            'message'   => 'Input Validation Failed!'
        ], HttpCode::UNPROCESSABLE_ENTITY));
    }
}
