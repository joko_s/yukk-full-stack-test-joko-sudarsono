<?php

namespace App\Core\Foundation\Http\Response;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use League\Fractal\Serializer\DataArraySerializer;

/**
 * Class DataSerializer
 *
 * @package App\Core\Foundation\Http\Response
 */
class DataSerializer extends DataArraySerializer
{
    const SUCCESS_MESSAGE_DEFAULT = 'success';

    /**
     * @var boolean $status
     */
    protected $status = true;

    /**
     * @var string $message
     */
    protected $message;

    protected $origin;

    /**
     *
     * @param null $origin
     * @param null $message
     * @param bool $status
     */
    public function __construct(
        $origin         = null,
        $message        = null,
        $status         = true
    
    ) {
        $this->origin       = $origin;
        $this->message      = $message ?? self::SUCCESS_MESSAGE_DEFAULT;
        $this->status       = $status;
    }

    /**
     * Serialize a collection.
     *
     * @param $resourceKey
     * @param array  $data
     *
     * @return array
     */
    public function collection($resourceKey, array $data): array
    {
        return [
            'status'        => $this->status,
            'message'       => $this->message,
            'data'          => $data,
            'pagination'    => $this->paginate(),
        ];
    }

    /**
     * @return array
     */
    protected function paginate(): array
    {
        if ($this->origin instanceof LengthAwarePaginator) {
            return [
                'total'         => $this->origin->total(),
                'per_page'      => $this->origin->perPage(),
                'current_page'  => $this->origin->currentPage(),
                'total_pages'   => $this->origin->lastPage(),
            ];
        }

        if (is_array($this->origin)) {
            return data_get($this->origin, 'pagination', []);
        }
        
        return [];
    }

    /**
     * Serialize an item.
     *
     * @param $resourceKey
     * @param array  $data
     *
     * @return array
     */
    public function item($resourceKey, array $data): array
    {
        return [
            'status'        => $this->status,
            'message'       => $this->message,
            'data'          => $data,
        ];
    }

    /**
     * Serialize null resource.
     *
     * @return array
     */
    public function null(): array
    {
        return [
            'status'        => $this->status,
            'message'       => $this->message,
            'data'          => [],
        ];
    }

    /**
     * Serialize the meta.
     *
     * @param array $meta
     *
     * @return array
     */
    public function meta(array $meta): array
    {
        if (empty($meta)) {
            return [];
        }

        // NOTE: To show meta ['meta' => $meta]
        return [];
    }
}