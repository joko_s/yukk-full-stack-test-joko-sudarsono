<?php

namespace App\Core\Foundation\Http\Response;

/**
 * Interface HttpCode
 *
 * @package Core\Foundation\Http
 */
interface HttpCode
{
    const OK = 200;
    const CREATED = 201;
    const NO_CONTENT = 204;

    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const UNPROCESSABLE_ENTITY = 422;

    const INTERNAL_SERVER = 500;
}
