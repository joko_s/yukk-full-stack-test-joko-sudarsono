<?php

namespace App\Core\Foundation\Http\Response;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\Core\Foundation\Http\Response\DataSerializer;

/**
 * Class SingleDataSerializer
 *
 * @package App\Core\Foundation\Http\Response
 */
class SingleDataSerializer extends DataSerializer
{
    /**
     * Serialize null resource.
     *
     * @return array
     */
    public function null(): array
    {
        return [
            'status'        => $this->status,
            'message'       => $this->message,
            'data'          => (object)[],
        ];
    }
}
