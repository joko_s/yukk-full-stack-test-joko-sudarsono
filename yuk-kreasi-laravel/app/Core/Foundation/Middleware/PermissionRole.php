<?php

namespace App\Core\Foundation\Middleware;

use Closure;
use Illuminate\Http\Request;

use App\Core\Foundation\Http\Response\HttpCode;

class PermissionRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        $user = auth()->user();

        $isAllowed = $user && !empty($user->getUserRole()) && in_array($user->getUserRole(), $roles);
        if (!$isAllowed) {
            return response()->json([
                'status' => false,
                'message' => 'Access Forbidden!'
            ], HttpCode::FORBIDDEN);
        }

        return $next($request);
    }
}
