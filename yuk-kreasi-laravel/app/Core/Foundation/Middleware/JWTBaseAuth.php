<?php

namespace App\Core\Foundation\Middleware;

use Closure;
use Illuminate\Http\Request;

use PHPOpenSourceSaver\JWTAuth\Http\Middleware\BaseMiddleware;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;
use PHPOpenSourceSaver\JWTAuth\Exceptions\JWTException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\TokenExpiredException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\TokenInvalidException;

use App\Core\Foundation\Http\Response\HttpCode;

class JWTBaseAuth extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            if ($e instanceof TokenInvalidException){
                return response()->json([
                    'status' => false,
                    'message' => 'Token is Invalid'
                ], HttpCode::UNAUTHORIZED);
            } elseif ($e instanceof TokenExpiredException){
                return response()->json([
                    'status' => false,
                    'message' => 'Token is Expired'
                ], HttpCode::UNAUTHORIZED);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Authorization Token not found'
                ], HttpCode::UNAUTHORIZED);
            }
        }

        return $next($request);
    }
}
