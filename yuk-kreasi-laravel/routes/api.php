<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\BalanceController;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => ['api']], function() {
    Route::group(['prefix' => 'auth'], function() {
        Route::post('/login', [UserController::class, 'login'])->name('user.login');
        Route::post('/register', [UserController::class, 'register'])->name('user.register');

        Route::group(['middleware' => ['jwt.base']], function() {
            Route::post('/logout', [UserController::class, 'logout'])->name('user.logout');
        });
    });

    Route::group(['prefix' => 'bank', 'middleware' => ['jwt.base']], function() {
        Route::get('/', [BankController::class, 'getAll'])->name('bank.getAll');
    });

    Route::group(['prefix' => 'balance', 'middleware' => ['jwt.base']], function() {
        Route::get('/', [BalanceController::class, 'balance'])->name('balance.detail')->middleware('role:2');
        Route::get('/histories', [BalanceController::class, 'histories'])->name('balance.histories');
    });

    Route::group(['prefix' => 'transactions', 'middleware' => ['jwt.base']], function() {
        Route::get('/', [TransactionController::class, 'getAll'])->name('transaction.getAll');
        Route::get('/{id}', [TransactionController::class, 'show'])->name('transaction.show');
        Route::post('/', [TransactionController::class, 'create'])->name('transaction.create')->middleware('role:2');
        Route::put('/', [TransactionController::class, 'update'])->name('transaction.update')->middleware('role:1');
    });
});
