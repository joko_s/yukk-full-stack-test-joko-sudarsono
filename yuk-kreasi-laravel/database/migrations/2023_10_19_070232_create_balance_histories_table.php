<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('balance_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('balance_id');
            $table->unsignedBigInteger('bank_id');
            $table->unsignedBigInteger('transaction_id');
            $table->string('account_name', 50);
            $table->string('account_number', 50);
            $table->tinyInteger('account_number_type');
            $table->unsignedBigInteger('amount');
            $table->unsignedBigInteger('previous_balance');
            $table->unsignedBigInteger('new_balance');
            $table->tinyInteger('transaction_type');
            $table->tinyInteger('status');
            $table->string('description', 150)->nullable();
            $table->timestamps();
        });

        Schema::table('balance_histories', function (Blueprint $table) {
            $table->foreign('balance_id', 'fk_balance_id_idx')
                ->references('id')->on('balance')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('bank_id', 'fk_bank_id_idx')
                ->references('id')->on('banks')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('transaction_id', 'bh_fk_transaction_id_idx')
                ->references('id')->on('transactions')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('balance_histories');
    }
};
