<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Core\Modules\Bank\Entity\Model\Bank;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Bank::insert($this->data());
    }

    private function data(): array
    {
        $now = \Carbon\Carbon::now();

        return [
            [
                'name'          => 'BCA',
                'bank_code'     => '001',
                'created_at'    => $now,
                'updated_at'    => $now
            ],
            [
                'name'          => 'Mandiri',
                'bank_code'     => '002',
                'created_at'    => $now,
                'updated_at'    => $now
            ],
            [
                'name'          => 'BNI',
                'bank_code'     => '003',
                'created_at'    => $now,
                'updated_at'    => $now
            ],
            [
                'name'          => 'OVO',
                'bank_code'     => '004',
                'created_at'    => $now,
                'updated_at'    => $now
            ],
            [
                'name'          => 'GOPAY',
                'bank_code'     => '005',
                'created_at'    => $now,
                'updated_at'    => $now
            ],
        ];
    }
}
