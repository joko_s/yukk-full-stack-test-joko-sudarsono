<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Hash;

use App\Core\Foundation\Entity\Model\Contract\UserInterface;
use App\Core\Foundation\Entity\Model\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::insert($this->data());
    }

    private function data(): array
    {
        $now = \Carbon\Carbon::now();

        return [
            [
                'name'          => 'Admin 1',
                'email'         => 'admin1@mail.com',
                'password'      => Hash::make('secret123'),
                'phone'         => '085111222333',
                'user_role'     => UserInterface::ROLE_ADMIN,
                'created_at'    => $now,
                'updated_at'    => $now
            ],
            [
                'name'          => 'User 1',
                'email'         => 'user1@mail.com',
                'password'      => Hash::make('secret123'),
                'phone'         => '085111222444',
                'user_role'     => UserInterface::ROLE_USER,
                'created_at'    => $now,
                'updated_at'    => $now
            ],
            [
                'name'          => 'User 2',
                'email'         => 'user2@mail.com',
                'password'      => Hash::make('secret123'),
                'phone'         => '085111222555',
                'user_role'     => UserInterface::ROLE_USER,
                'created_at'    => $now,
                'updated_at'    => $now
            ],
            [
                'name'          => 'User 3',
                'email'         => 'user3@mail.com',
                'password'      => Hash::make('secret123'),
                'phone'         => '085111222666',
                'user_role'     => UserInterface::ROLE_USER,
                'created_at'    => $now,
                'updated_at'    => $now
            ],
        ];
    }
}
