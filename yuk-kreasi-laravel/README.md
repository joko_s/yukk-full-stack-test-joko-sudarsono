## Requirements
- PHP version `8.1` or later
- MariaDB version `15` or later

## Preparations
```
$ cp .env.example .env
$ php artisan key:generate
$ php artisan migrate
$ php artisan db:seed --class=UserTableSeeder
$ php artisan db:seed --class=BankTableSeeder
```

## Running Program
```
$ php artisan serve
```

## Api Docs
api docs available in postman collection inside folder `postman` on the root of this repository

## Default Admin User
email: ```admin1@mail.com```
password: ```secret123```

the purpose of admin user is to handle update transaction status. this approach is to simulate update transaction status by third party payment system.