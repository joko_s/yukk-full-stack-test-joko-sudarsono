## Requirments
- node.js version ```v18.17.0``` or later

## Getting Started
```
$ cp .env.sample .env
$ yarn install
$ yarn start
```

## Production build
```
$ make build-prod
$ make run-prod
```
application running on ```http://localhost:3000```
