import { useRoutes } from 'react-router-dom';
import { PublicRoute, PrivateRoute } from 'components/handler';

// account page
import RegisterPage from 'pages/account/RegisterPage';
import LoginPage from 'pages/account/LoginPage';

// balance
import BalancePage from 'pages/balance/BalancePage';

// bank
import BankPage from 'pages/bank/BankPage';

// transaction
import TransactionPage from 'pages/transaction/TransactionPage';
import TransactionDetailPage from 'pages/transaction/TransactionDetailPage';
import CreateTransactionPage from 'pages/transaction/CreateTransactionPage';

// default
import Page404 from 'pages/Page404';

/**
 * NOTES
 * 1. gunakan wrapper <PublicRoute /> hanya untuk route yang membutuhkan untuk strict redirect ke private route
 */
export const PUBLIC_ROUTES = [
  {
    path: '/',
    element: <LoginPage />,
  },
  {
    path: '/register',
    element: <RegisterPage />,
  },

  /** private Route */
  {
    path: '/balance',
    element: (
      <PrivateRoute>
        <BalancePage />
      </PrivateRoute>
    ),
  },
  {
    path: '/bank',
    element: (
      <PrivateRoute>
        <BankPage />
      </PrivateRoute>
    ),
  },
  {
    path: '/transaction',
    element: (
      <PrivateRoute>
        <TransactionPage />
      </PrivateRoute>
    ),
  },
  {
    path: '/transaction/create',
    element: (
      <PrivateRoute>
        <CreateTransactionPage />
      </PrivateRoute>
    ),
  },
  {
    path: '/transaction/:transactionId',
    element: (
      <PrivateRoute>
        <TransactionDetailPage />
      </PrivateRoute>
    ),
  },
];

export const PRIVATE_ROUTES = [];

const OTHER_ROUTES = [{ path: '*', element: <Page404 /> }];

// export const routes = Object.values({ ...PRIVATE_ROUTES, ...PUBLIC_ROUTES });
export const routes = [...PUBLIC_ROUTES, ...PRIVATE_ROUTES, ...OTHER_ROUTES];

// initialize route
const RouteElement = (props) => {
  const elements = useRoutes(routes);
  return elements;
};

export default RouteElement;
