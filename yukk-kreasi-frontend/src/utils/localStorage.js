const tokenName = 'ustn';
const userKey = 'usr';
const langKey = 'lng';

export const setLang = (lang) => {
  window.localStorage.setItem(langKey, lang);
};

export const getLang = () => {
  const lang = window.localStorage.getItem(langKey);
  return lang;
};

/** User Token */
export const setUserTokenStorage = (token) => {
  window.localStorage.setItem(tokenName, token);
};

export const getUserTokenStorage = () => {
  const token = window.localStorage.getItem(tokenName);
  return token;
};

export const clearUserTokenStorage = () => {
  window.localStorage.removeItem(tokenName);
};

/** User Data */
export const setUserDataStorage = (data) => {
  window.localStorage.setItem(userKey, JSON.stringify(data));
};

export const getUserDataStorage = () => {
  const data = window.localStorage.getItem(userKey);
  return JSON.parse(data);
};

export const clearUserDataStorage = () => {
  window.localStorage.removeItem(userKey);
};
