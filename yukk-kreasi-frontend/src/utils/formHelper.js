import _get from 'lodash/get';

export const isFieldError = (key, clientErr, serverErr) => {
  const checkClientErr = _get(clientErr, `${key}.message`) || '';
  const checkServerErr = _get(serverErr, `${key}.[0]`) || '';

  if (checkClientErr || checkServerErr) return true;
  return false;
};

export const getErrorMessage = (key, clientErr, serverErr) => {
  const checkClientErr = _get(clientErr, `${key}.message`) || '';
  const checkServerErr = _get(serverErr, `${key}.[0]`) || '';

  // prioritas menampilkan server error
  if (checkServerErr) return checkServerErr;
  if (checkClientErr) return checkClientErr;

  return '';
};
