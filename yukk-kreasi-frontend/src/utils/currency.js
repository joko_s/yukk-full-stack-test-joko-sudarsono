import numeral from 'numeral';

export const toIDR = (numb) => {
  if (numb === 0 || numb === '0') return `Rp. 0`;
  if (!numb) return '';

  return `Rp. ${numeral(numb).format('0,0')}`;
};
