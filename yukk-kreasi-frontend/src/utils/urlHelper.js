import _get from 'lodash/get';

export const getUrlSegment = (segment) => {
  const path = window.location.pathname;
  const splitted = path.split('/');

  const result = _get(splitted, `${segment}`) || '';
  return result;
};
