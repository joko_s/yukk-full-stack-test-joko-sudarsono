export const DateToHumanDate = (date = null, region = 'en') => {
  if (!date) return null;

  const formatted = Intl.DateTimeFormat(region, {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    hour12: false,
  }).format(new Date(date));

  return formatted;
};
