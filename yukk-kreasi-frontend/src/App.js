import { jsx, ThemeProvider } from '@emotion/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import { theme } from 'components/themes/v1';

import RouteElement from 'routes/RouteElement';

import 'assets/styles/app.scss';

import i18n from 'libs/i18n';
import useInitFetch from 'hooks/useInitFetch';

// i18n init
i18n();

const App = (props) => {
  // default api requests on initial load
  useInitFetch();

  return (
    <ThemeProvider theme={theme}>
      {/* Init Toast */}
      <ToastContainer
        position='top-right'
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={true}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        pauseOnHover
      />

      {/* Init router */}
      <Router>
        <RouteElement />
      </Router>
    </ThemeProvider>
  );
};

export default App;
