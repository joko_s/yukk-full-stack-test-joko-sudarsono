import api from 'libs/api';
import { auth as authUrl } from './endpoint';

export const userLogin = async (data, config = {}) => {
  const resp = await api().post(authUrl.login, data, config);
  return resp;
};

export const userRegister = async (data, config = {}) => {
  const resp = await api().post(authUrl.register, data, config);
  return resp;
};

export const userLogout = async (config = {}) => {
  const resp = await api().post(authUrl.logout, config);
  return resp;
};
