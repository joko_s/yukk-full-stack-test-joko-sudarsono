import _template from 'lodash/template';
import api from 'libs/api';
import { balance as balanceUrl } from './endpoint';

export const getBalance = async (config = {}) => {
  const resp = await api().get(balanceUrl.index, config);
  return resp;
};

export const getBalanceHistories = async (params, config = {}) => {
  const resp = await api().get(balanceUrl.histories, { params, ...config });
  return resp;
};
