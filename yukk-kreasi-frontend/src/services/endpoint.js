/**
 * This file contains all api urls
 */
export const auth = {
  login: '/api/auth/login',
  register: '/api/auth/register',
  logout: '/api/auth/logout',
};

export const bank = {
  index: '/api/bank',
};

export const balance = {
  index: '/api/balance',
  histories: '/api/balance/histories',
};

export const transaction = {
  index: '/api/transactions',
  detail: '/api/transactions/${id}',
};
