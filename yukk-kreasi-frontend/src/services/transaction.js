import _template from 'lodash/template';
import api from 'libs/api';
import { transaction as transactionUrl } from './endpoint';

export const getTransaction = async (params, config = {}) => {
  const resp = await api().get(transactionUrl.index, { params, ...config });
  return resp;
};

export const getTransactionDetail = async (id, config = {}) => {
  const url = _template(transactionUrl.detail)({ id });
  const resp = await api().get(url, config);
  return resp;
};

export const createTransaction = async (data, config = {}) => {
  const resp = await api().post(transactionUrl.index, data, config);
  return resp;
};

export const updateTransaction = async (data, config = {}) => {
  const resp = await api().put(transactionUrl.index, data, config);
  return resp;
};
