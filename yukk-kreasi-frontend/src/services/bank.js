import _template from 'lodash/template';
import api from 'libs/api';
import { bank as bankUrl } from './endpoint';

export const getBank = async (config = {}) => {
  const resp = await api().get(bankUrl.index, config);
  return resp;
};
