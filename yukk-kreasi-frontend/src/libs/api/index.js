import axios from 'axios';
import _get from 'lodash/get';

import {
  getUserTokenStorage,
  clearUserTokenStorage,
  clearUserDataStorage,
} from 'utils/localStorage';

const defaultConfig = {
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
};

const api = (customConfig, transformResp = true) => {
  const token = getUserTokenStorage();

  if (token) defaultConfig.headers.Authorization = `Bearer ${token}`;

  const client = axios.create({
    ...defaultConfig,
    ...(typeof customConfig === 'object' ? customConfig : {}),
  });

  client.interceptors.response.use(
    (res) => (transformResp ? transformResponse(res) : res),
    async (error) => {
      if (axios.isCancel(error)) return transformError(error);

      const prevRequest = error.config;

      const status = _get(error, 'response.status') || 400;

      if (status === 403) {
        return transformError(error);
      }

      if (status === 401) {
        return transformError(error, true);
      }

      // do refresh token
      // if (status === 401 && !prevRequest._retry) {
      //     prevRequest['_retry'] = true;
      //     const refreshToken = getSession('rut');
      //     if (!refreshToken) return transformError(error, false);
      // }

      return transformError(error);
    }
  );

  return client;
};

const transformError = (error, clearAndRedirect) => {
  if (axios.isCancel(error)) {
    return {
      data: null,
      status: 499,
      statusText: 'Client Closed Request',
      headers: {},
      config: {},
      success: false,
      message: 'Client Closed Request',
    };
  }

  if (clearAndRedirect) {
    clearUserTokenStorage();
    clearUserDataStorage();

    setTimeout(() => {
      window.location.href = '/';
    }, 250);
  }

  return {
    data: _get(error, 'response.data') || null,
    status: _get(error, 'response.status') || 400,
    statusText: _get(error, 'response.statusText') || 'Bad request',
    headers: _get(error, 'response.headers') || {},
    config: _get(error, 'response.config') || error.config,
    success: false,
    message: _get(error, 'response.data.message', 'An error has occurred while processing your request.'),
  };
};

const transformResponse = (response) => {
  const data = _get(response, 'data.data', {});
  const pagination = _get(response, 'data.pagination', {});
  const status = _get(response, 'status', 400);
  const msg = axios.isCancel(response) ? 'message' : 'data.message';
  const message = _get(response, msg, '');

  let result = {
    data,
    message,
    success: status >= 200 && status < 300,
  }

  if (Object.keys(pagination).length > 0) {
    result = {
      ...result,
      pagination
    }
  }

  return result;
};

export default api;
