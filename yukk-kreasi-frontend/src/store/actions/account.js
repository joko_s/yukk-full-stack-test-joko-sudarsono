import { ACCOUNT_SET_PROFILE } from 'store/types';

export const storeAccountProfile = (data) => {
  return {
    type: ACCOUNT_SET_PROFILE,
    payload: data,
  };
};
