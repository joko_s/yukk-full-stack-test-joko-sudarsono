import { BALANCE_SET_DETAIL, BALANCE_SET_HISTORY_DATA } from 'store/types';

export const storeBalanceDetail = (data) => {
  return {
    type: BALANCE_SET_DETAIL,
    payload: data,
  };
};

export const storeBalanceHistoryData = (data) => {
  return {
    type: BALANCE_SET_HISTORY_DATA,
    payload: data,
  };
};
