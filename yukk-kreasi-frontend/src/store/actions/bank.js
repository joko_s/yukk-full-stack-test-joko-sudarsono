import { BANK_SET_DATA } from 'store/types';

export const storeBankData = (data) => {
  return {
    type: BANK_SET_DATA,
    payload: data,
  };
};
