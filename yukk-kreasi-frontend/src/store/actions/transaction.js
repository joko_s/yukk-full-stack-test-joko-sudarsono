import { TRANSACTION_SET_DATA, TRANSACTION_SET_DETAIL } from 'store/types';

export const storeTransactionData = (data) => {
  return {
    type: TRANSACTION_SET_DATA,
    payload: data,
  };
};

export const storeTransactionDetail = (data) => {
  return {
    type: TRANSACTION_SET_DETAIL,
    payload: data,
  };
};
