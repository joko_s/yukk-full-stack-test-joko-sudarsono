export default {
  lang: 'id',
  account: {
    detail: {},
  },
  bank: {
    list: {
      data: [],
    },
  },
  balance: {
    detail: {},
    histories: {
      data: [],
      pagination: {
        current_page: 1,
        total_pages: 1,
        per_page: 10,
        total: 1,
      },
    },
  },
  transaction: {
    list: {
      data: [],
      pagination: {
        current_page: 1,
        total_pages: 1,
        per_page: 10,
        total: 1,
      },
    },
    detail: {},
  },
};
