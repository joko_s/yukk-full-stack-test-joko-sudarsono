import update from 'immutability-helper';

import initialState from 'store/initialState';
import { createReducer } from 'store/reduxTools';
import { BANK_SET_DATA } from 'store/types';

export default createReducer(initialState, {
  [BANK_SET_DATA]: (state, action) => bankDataUpdate(state, action.payload),
});

const bankDataUpdate = (state, payload) => {
  const { data } = payload;

  return update(state, {
    bank: {
      list: {
        data: { $set: data },
      },
    },
  });
};
