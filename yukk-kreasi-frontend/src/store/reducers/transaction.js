import update from 'immutability-helper';

import initialState from 'store/initialState';
import { createReducer } from 'store/reduxTools';
import { TRANSACTION_SET_DATA, TRANSACTION_SET_DETAIL } from 'store/types';

export default createReducer(initialState, {
  [TRANSACTION_SET_DATA]: (state, action) => transactionDataUpdate(state, action.payload),
  [TRANSACTION_SET_DETAIL]: (state, action) => transactionDetailUpdate(state, action.payload),
});

const transactionDataUpdate = (state, payload) => {
  const { data, pagination } = payload;

  return update(state, {
    transaction: {
      list: {
        data: { $set: data },
        pagination: { $set: pagination },
      },
    },
  });
};

const transactionDetailUpdate = (state, payload) => {
  const data = payload;

  return update(state, {
    transaction: {
      detail: { $set: data },
    },
  });
};
