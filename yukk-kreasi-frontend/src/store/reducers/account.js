import update from 'immutability-helper';

import initialState from 'store/initialState';
import { createReducer } from 'store/reduxTools';
import { ACCOUNT_SET_PROFILE } from 'store/types';

export default createReducer(initialState, {
  [ACCOUNT_SET_PROFILE]: (state, action) => accountProfileUpdate(state, action.payload),
});

const accountProfileUpdate = (state, payload) => {
  return update(state, {
    account: {
      detail: { $set: payload },
    },
  });
};
