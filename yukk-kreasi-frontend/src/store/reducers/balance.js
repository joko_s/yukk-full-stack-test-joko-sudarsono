import update from 'immutability-helper';

import initialState from 'store/initialState';
import { createReducer } from 'store/reduxTools';
import { BALANCE_SET_DETAIL, BALANCE_SET_HISTORY_DATA } from 'store/types';

export default createReducer(initialState, {
  [BALANCE_SET_DETAIL]: (state, action) => balanceDetailUpdate(state, action.payload),
  [BALANCE_SET_HISTORY_DATA]: (state, action) => balanceHistoryDataUpdate(state, action.payload),
});

const balanceDetailUpdate = (state, payload) => {
  const { data } = payload;

  return update(state, {
    balance: {
      detail: { $set: data },
    },
  });
};

const balanceHistoryDataUpdate = (state, payload) => {
  const { data, pagination } = payload;

  return update(state, {
    balance: {
      histories: {
        data: { $set: data },
        pagination: { $set: pagination },
      },
    },
  });
};
