import { reduceReducers } from 'store/reduxTools';
import initialState from 'store/initialState';

import lang from './lang';
import account from './account';
import balance from './balance';
import bank from './bank';
import transaction from './transaction';

const reducers = reduceReducers(initialState, account, balance, bank, transaction, lang);

export default reducers;
