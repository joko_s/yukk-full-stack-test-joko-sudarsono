import _get from 'lodash/get';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { getUserDataStorage, getLang } from 'utils/localStorage';
import { storeAccountProfile } from 'store/actions/account';
import { storeLang } from 'store/actions/lang';

const useInitFetch = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    // fetch user account
    (async () => {
      // set lang
      const currLang = getLang();
      const lang = currLang ? currLang : 'en';
      dispatch(storeLang(lang));

      // set user from local storage
      const user = getUserDataStorage();
      if (user) dispatch(storeAccountProfile(user));

    })();
  }, [dispatch]);
};

export default useInitFetch;
