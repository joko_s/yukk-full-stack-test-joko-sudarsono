export const ROLE_ADMIN = 1;
export const ROLE_USER = 2;

export const USER_ROLES = {
  [ROLE_ADMIN]: 'role.admin',
  [ROLE_USER]: 'role.candidate',
};

export const getRoleName = (roleId) => {
  if (USER_ROLES[roleId]) {
    return USER_ROLES[roleId];
  }

  return '';
};
