
export const ACCOUNT_NUMBER_TYPE_BANK = 1;
export const ACCOUNT_NUMBER_TYPE_VA = 2;

export const ACCOUNT_NUMBER_TYPES = [
  ACCOUNT_NUMBER_TYPE_BANK,
  ACCOUNT_NUMBER_TYPE_VA,
];

export const ACCOUNT_NUMBER_TYPE_OPT = {
  [ACCOUNT_NUMBER_TYPE_BANK]: 'Bank Account',
  [ACCOUNT_NUMBER_TYPE_VA]: 'Virtual Account',

}

export const ACCOUNT_NUMBER_TYPE_OPTIONS = [
  { id: ACCOUNT_NUMBER_TYPE_BANK, value: 'Bank Account'},
  { id: ACCOUNT_NUMBER_TYPE_VA, value: 'Virtual Account'},
]

export const TRANSACTION_TYPE_TOPUP = 1;
export const TRANSACTION_TYPE_TRANSACTION = 2;

export const TRANSACTION_TYPES = [
  TRANSACTION_TYPE_TOPUP,
  TRANSACTION_TYPE_TRANSACTION,
];

export const TRANSACTION_TYPE_OPT = {
  [TRANSACTION_TYPE_TOPUP]: 'Top Up',
  [TRANSACTION_TYPE_TRANSACTION]: 'Transaction',
}

export const TRANSACTION_TYPE_OPTIONS = [
  { id: TRANSACTION_TYPE_TOPUP, value: 'Top Up'},
  { id: TRANSACTION_TYPE_TRANSACTION, value: 'Transaction'},
]

export const STATUS_PENDING = 1;
export const STATUS_PROCESSED = 2;
export const STATUS_SETTLED = 3;
export const STATUS_REJECTED = 4;

export const STATUS_PENDING_TITLE = 'Pending';
export const STATUS_PROCESSED_TITLE = 'Processed';
export const STATUS_SETTLED_TITLE = 'Settled';
export const STATUS_REJECTED_TITLE = 'Rejected';

export const STATUS_LIST = [
  STATUS_PENDING,
  STATUS_PROCESSED,
  STATUS_SETTLED,
  STATUS_REJECTED,
];

export const STATUS_OPT = {
  [STATUS_PENDING]: STATUS_PENDING_TITLE,
  [STATUS_PROCESSED]: STATUS_PROCESSED_TITLE,
  [STATUS_SETTLED]: STATUS_SETTLED_TITLE,
  [STATUS_REJECTED]: STATUS_REJECTED_TITLE,
}

export const STATUS_OPTIONS = [
  { id: STATUS_PENDING, value: STATUS_PENDING_TITLE},
  { id: STATUS_PROCESSED, value: STATUS_PROCESSED_TITLE},
  { id: STATUS_SETTLED, value: STATUS_SETTLED_TITLE},
  { id: STATUS_REJECTED, value: STATUS_REJECTED_TITLE},
]

export const BALANCE_STATUS_LIST = [
  STATUS_PENDING,
  STATUS_SETTLED,
  STATUS_REJECTED,
];

export const BALANCE_STATUS_OPT = {
  [STATUS_PENDING]: STATUS_PENDING_TITLE,
  [STATUS_SETTLED]: STATUS_SETTLED_TITLE,
  [STATUS_REJECTED]: STATUS_REJECTED_TITLE,
}

export const BALANCE_STATUS_OPTIONS = [
  { id: STATUS_PENDING, value: STATUS_PENDING_TITLE},
  { id: STATUS_SETTLED, value: STATUS_SETTLED_TITLE},
  { id: STATUS_REJECTED, value: STATUS_REJECTED_TITLE},
]

export const getAccountNumberType = (type) => {
  if (ACCOUNT_NUMBER_TYPE_OPT[type]) {
    return ACCOUNT_NUMBER_TYPE_OPT[type];
  }

  return '';
};

export const getTransactionType = (type) => {
  if (TRANSACTION_TYPE_OPT[type]) {
    return TRANSACTION_TYPE_OPT[type];
  }

  return '';
};

export const getBalanceStatus = (status) => {
  if (BALANCE_STATUS_OPT[status]) {
    return BALANCE_STATUS_OPT[status];
  }

  return '';
};

export const getTransactionStatus = (status) => {
  if (STATUS_OPT[status]) {
    return STATUS_OPT[status];
  }

  return '';
};
