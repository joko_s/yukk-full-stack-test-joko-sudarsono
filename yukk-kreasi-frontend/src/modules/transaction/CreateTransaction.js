import _get from 'lodash/get';
import styled from '@emotion/styled';
import Pagination from 'react-bootstrap-4-pagination';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router';
import { toast } from 'react-toastify';
import { Container, Row, Col, Form, Button, Card, Spinner, Modal, Table } from 'react-bootstrap';
import { PlusLg } from 'react-bootstrap-icons';

import { DateToHumanDate } from 'utils/datetime'
import { getAccountNumberType, getTransactionType, getBalanceStatus, getTransactionStatus } from 'constants/transaction'

import { getBank } from 'services/bank'
import { getTransactionDetail, createTransaction } from 'services/transaction'
import { storeTransactionDetail } from 'store/actions/transaction'

import { isFieldError, getErrorMessage } from 'utils/formHelper';
import { FormInvalidFeedback, BasicSelect } from 'components/common/form';
import {
  ACCOUNT_NUMBER_TYPE_OPTIONS,
  TRANSACTION_TYPE_OPTIONS,
  STATUS_OPTIONS,
  BALANCE_STATUS_OPTIONS,
} from 'constants/transaction'

import ItemDetail from './ItemDetail'

const CreateTransaction = (props) => {
  const [formLoading, setFormLoading] = useState(false);
  const [state, setState] = useState({
    bankOpt: [],
  })

  const { detail } = useSelector((state) => state.transaction);
  const histories = _get(detail, 'histories', [])

  const { t } = useTranslation();
  const params = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
    getValues,
  } = useForm();

  const onSubmit = async (values) => {
    setFormLoading(true);
    const payload = {
      ...values,
      bank_id: parseInt(values.bank_id),
      account_number_type: parseInt(values.account_number_type),
      transaction_type: parseInt(values.transaction_type),
    };

    const resp = await createTransaction(payload);
    setFormLoading(false);

    if (resp.success) {
      toast.success(t('saved'));

      setTimeout(() => {
        navigate('/transaction');
      }, 500);
    } else {
      const errMsg = _get(resp, 'data.message') || t('failed');
      toast.error(errMsg);
    }
  };

  useEffect(() => {
    register('bank_id');
    register('account_number_type');
    register('transaction_type');

    (async () => {
      const resp = await getBank()
      const bankOpt = _get(resp, 'data', [])
      setState(prev => ({ ...prev, bankOpt }))
    })();
  }, []);

  return (
    <Container className='mb-4 pt-4'>
      <Row className='mb-4'>
        <Col>
          <Card>
            <Card.Header>
              {t('button.add_new')}
            </Card.Header>

            <Card.Body>
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group controlId=''>
                  <Form.Label>{t('form.bank')}</Form.Label>

                  <BasicSelect
                    id='bankOpt'
                    placeholder={t('form.bank')}
                    options={state.bankOpt}
                    value={getValues('bank_id')}
                    onChange={(val) => {
                      setState((prev) => ({ ...prev, bank_id: val }));
                      setValue('bank_id', val);
                    }}
                  />
                </Form.Group>

                <Form.Group className='mb-3'>
                  <Form.Label>{t('form.account_name')}</Form.Label>

                  <Form.Control type='text' placeholder={t('form.account_name')} {...register('account_name')} />
                </Form.Group>

                <Form.Group className='mb-3'>
                  <Form.Label>{t('form.account_number')}</Form.Label>

                  <Form.Control type='text' placeholder={t('form.account_number')} {...register('account_number')} />
                </Form.Group>

                <Form.Group>
                  <Form.Label>{t('form.account_number_type')}</Form.Label>

                  <BasicSelect
                    id='accNumbOpt'
                    placeholder={t('form.account_number_type')}
                    options={ACCOUNT_NUMBER_TYPE_OPTIONS}
                    value={getValues('account_number_type')}
                    keys={{ name: 'value' }}
                    onChange={(val) => {
                      setState((prev) => ({ ...prev, account_number_type: val }));
                      setValue('account_number_type', val);
                    }}
                  />
                </Form.Group>

                <Form.Group className='mb-3'>
                  <Form.Label>{t('form.amount')}</Form.Label>

                  <Form.Control type='number' placeholder={t('form.amount')} {...register('amount')} />
                </Form.Group>

                <Form.Group>
                  <Form.Label>{t('form.transaction_type')}</Form.Label>

                  <BasicSelect
                    id='accNumbOpt'
                    placeholder={t('form.transaction_type')}
                    options={TRANSACTION_TYPE_OPTIONS}
                    value={getValues('transaction_type')}
                    keys={{ name: 'value' }}
                    onChange={(val) => {
                      setState((prev) => ({ ...prev, transaction_type: val }));
                      setValue('transaction_type', val);
                    }}
                  />
                </Form.Group>

                <Form.Group className='mb-3'>
                  <Form.Label>{t('form.description')}</Form.Label>

                  <Form.Control type='text' placeholder={t('form.description')} {...register('description')} />
                </Form.Group>

                <Form.Group>
                  <Button variant='primary' type='submit'>
                    {formLoading ? <Spinner animation='border' variant='light' size='sm' /> : null} {t('button.submit')}
                  </Button>
                </Form.Group>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default CreateTransaction;
