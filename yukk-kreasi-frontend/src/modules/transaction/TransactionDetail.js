import _get from 'lodash/get';
import styled from '@emotion/styled';
import Pagination from 'react-bootstrap-4-pagination';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router';
import { toast } from 'react-toastify';
import { Container, Row, Col, Form, Button, Card, Spinner, Modal, Table } from 'react-bootstrap';
import { PlusLg } from 'react-bootstrap-icons';

import { DateToHumanDate } from 'utils/datetime'
import { getAccountNumberType, getTransactionType, getBalanceStatus, getTransactionStatus } from 'constants/transaction'

import { getTransactionDetail, updateTransaction } from 'services/transaction'
import { storeTransactionDetail } from 'store/actions/transaction'

import { isFieldError, getErrorMessage } from 'utils/formHelper';
import { FormInvalidFeedback, BasicSelect } from 'components/common/form';
import {
  ACCOUNT_NUMBER_TYPE_OPTIONS,
  TRANSACTION_TYPE_OPTIONS,
  STATUS_OPTIONS,
  BALANCE_STATUS_OPTIONS,
} from 'constants/transaction'

import ItemDetail from './ItemDetail'

const TransactionDetail = (props) => {
  const [pageLoading, setPageLoading] = useState(false);
  const [formLoading, setFormLoading] = useState(false);
  const [updateModal, setUpdateModal] = useState(false);
  const [state, setState] = useState({
    status: '',
    notes: ''
  })

  const { detail } = useSelector((state) => state.transaction);
  const histories = _get(detail, 'histories', [])

  const { t } = useTranslation();
  const params = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
    getValues,
  } = useForm();

  const fetchTransactionDetail = async (id) => {
    const resp = await getTransactionDetail(id);

    if (resp.success) {
      const data = _get(resp, 'data') || [];
      const status = _get(data, 'status', '')
      const notes = _get(data, 'notes', '')

      setState(prev => ({
        ...prev,
        status,
        notes,
      }))

      setValue('transaction_id', id);
      setValue('status', status);
      setValue('notes', notes);

      dispatch(storeTransactionDetail(data));
    }
  };

  const onSubmit = async (values) => {
    setFormLoading(true);
    const resp = await updateTransaction(values);
    setFormLoading(false);

    if (resp.success) {
      setUpdateModal(false)
      toast.success(t('saved'));

      const data = _get(resp, 'data') || [];
      dispatch(storeTransactionDetail(data));
    } else {
      const errMsg = _get(resp, 'data.message') || t('failed');
      toast.error(errMsg);
    }
  };

  useEffect(() => {
    register('transaction_id');
    register('status');

    (async () => {
      setPageLoading(true);
      const resp = await fetchTransactionDetail(params.transactionId)
      setPageLoading(false);
    })();
  }, [params.transactionId]);

  return (
    <Container className='mb-4 pt-4'>
      <Row className='mb-4'>
        <Col>
          <Card>
            <Card.Body>
              {!pageLoading && !!detail && Object.keys(detail).length > 0
                ? <ItemDetail data={detail} onUpdate={() => setUpdateModal(true)}/>
                : null}

              {pageLoading ? (
                <div className='text-center my-4'>
                  <Spinner animation='border' variant='primary' />
                </div>
              ) : null}
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <Row className='mb-4'>
        <Col>
          <Card>
            <Card.Body>
              <Table striped hover>
                <thead>
                  <tr>
                    <th width='20%'>{t('status')}</th>
                    <th width='60%'>{t('notes')}</th>
                    <th width='20%'>{t('created_at')}</th>
                  </tr>
                </thead>

                <tbody>
                  {histories.length > 0
                    ? histories.map((item, idx) => {
                        return (
                          <tr key={`status${idx}`}>
                            <td>{getTransactionStatus(item.status)}</td>
                            <td>{item.notes}</td>
                            <td>{DateToHumanDate(item.created_at)}</td>
                          </tr>
                        );
                      })
                    : null}
                </tbody>
              </Table>
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <Modal show={updateModal} onHide={() => setUpdateModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{t('update')}</Modal.Title>
        </Modal.Header>

        <Form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Body>

            <Form.Group controlId=''>
              <Form.Label>{t('form.status')}</Form.Label>

              <BasicSelect
                id='statusOpt'
                placeholder={t('form.status')}
                options={STATUS_OPTIONS}
                value={getValues('status')}
                keys={{ name: 'value' }}
                onChange={(val) => {
                  setState((prev) => ({ ...prev, status: val }));
                  setValue('status', val);
                }}
              />
            </Form.Group>

            <Form.Group className='mb-3' controlId='exampleForm.ControlInput1'>
              <Form.Label>{t('form.notes')}</Form.Label>

              <Form.Control type='text' placeholder={t('form.notes')} {...register('notes')} />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant='secondary' onClick={() => setUpdateModal(false)}>
              {t('button.cancel')}
            </Button>

            <Button variant='primary' type='submit'>
              {formLoading ? <Spinner animation='border' variant='light' size='sm' /> : null} {t('button.submit')}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </Container>
  );
};

export default TransactionDetail;
