import _get from 'lodash/get';
import styled from '@emotion/styled';
import { Row, Col, Button } from 'react-bootstrap'
import { useTranslation } from 'react-i18next';
import { toIDR } from 'utils/currency';
import { DateToHumanDate } from 'utils/datetime'

import { getAccountNumberType, getTransactionType, getBalanceStatus, getTransactionStatus } from 'constants/transaction'

const ItemContainer = styled.div`
  margin-top: 12px;
  padding: 12px 0;
  border-bottom: 1px solid #bbbaba;

  &:last-child {
    border: none;
  }
`;

const ItemDetail = ({ data, onDetail }) => {
  const { t } = useTranslation();
  const accountName = _get(data, 'account_name', '')
  const accountNumber = _get(data, 'account_number', '')
  const accountNumberType = _get(data, 'account_number_type', '')
  const amount = _get(data, 'amount', '')
  const previousBalance = _get(data, 'previous_balance', '')
  const newBalance = _get(data, 'new_balance', '')
  const transactionType = _get(data, 'transaction_type', '')
  const status = _get(data, 'status', '')
  const description = _get(data, 'description', '')
  const createdAt = _get(data, 'created_at', '')

  return (
    <ItemContainer>
      <Row>
        <Col>
          <h4 className='font-weight-bold'>
            {`${getTransactionType(transactionType)} ${t('to')}: `}
            <span className='text-secondary text-uppercase'>{`${accountName} - ${accountNumber}`}</span>
          </h4>
        </Col>
      </Row>

      <Row>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('amount')}:</div>
          <h5 className='font-weight-bold'>{toIDR(amount)}</h5>
        </Col>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('previous_balance')}:</div>
          <h5 className='font-weight-bold'>{toIDR(previousBalance)}</h5>
        </Col>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('new_balance')}:</div>
          <h5 className='font-weight-bold'>{toIDR(newBalance)}</h5>
        </Col>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('created_at')}:</div>
          <h5 className='font-weight-bold'>{DateToHumanDate(createdAt)}</h5>
        </Col>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('account_name')}:</div>
          <h5 className='font-weight-bold'>{accountName}</h5>
        </Col>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('account_number')}:</div>
          <h5 className='font-weight-bold'>{accountNumber}</h5>
        </Col>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('account_number_type')}:</div>
          <h5 className='font-weight-bold'>{getAccountNumberType(accountNumberType)}</h5>
        </Col>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('transaction_type')}:</div>
          <h5 className='font-weight-bold'>{getTransactionType(transactionType)}</h5>
        </Col>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('status')}:</div>
          <h5 className='font-weight-bold'>{getBalanceStatus(status)}</h5>
        </Col>
        <Col md='3' sm='4'>
          <div className='text-secondary'>{t('description')}:</div>
          <h5 className='font-weight-bold'>{description}</h5>
        </Col>
      </Row>
    </ItemContainer>
  )
}

export default ItemDetail;
