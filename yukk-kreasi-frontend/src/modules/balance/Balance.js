import _get from 'lodash/get';
import styled from '@emotion/styled';
import Pagination from 'react-bootstrap-4-pagination';
import { useTranslation } from 'react-i18next';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { toast } from 'react-toastify';
import { Container, Row, Col, Form, Button, Card, Spinner, Modal } from 'react-bootstrap';
import { PlusLg } from 'react-bootstrap-icons';

import { storeBalanceDetail, storeBalanceHistoryData } from 'store/actions/balance';
import { getBalance, getBalanceHistories } from 'services/balance';

import { toIDR } from 'utils/currency';
import { SinglePagination } from 'components/common';
import {
  ACCOUNT_NUMBER_TYPE_OPTIONS,
  TRANSACTION_TYPE_OPTIONS,
  STATUS_OPTIONS,
  BALANCE_STATUS_OPTIONS,
} from 'constants/transaction'

import { PermissionRole } from 'components/handler'
import { ROLE_USER } from 'constants/role'

import ItemDetail from './ItemDetail'

const Balance = (props) => {
  const [pageLoading, setPageLoading] = useState(false);
  const [queryParams, setQueryParams] = useState({ page: 1, per_page: 10 });
  const [state, setState] = useState({
    search: '',
    account_number_type: '',
    transaction_type: '',
    status: '',
  });

  const { detail } = useSelector((state) => state.balance);
  const { data, pagination } = useSelector((state) => state.balance.histories);
  const amount = _get(detail, 'amount', 0)

  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const fetchBalance = async () => {
    const resp = await getBalance();

    if (resp.success) {
      const data = _get(resp, 'data') || [];
      dispatch(storeBalanceDetail({ data }));
    }
  };

  const fetchBalanceHistories = async (params) => {
    const resp = await getBalanceHistories(params);
    if (resp.success) {
      const current_page = _get(resp, 'pagination.current_page') || 1;
      const total_pages = _get(resp, 'pagination.total_pages') || 1;
      const per_page = _get(resp, 'pagination.per_page') || 1;
      const total = _get(resp, 'pagination.total') || 10;
      const pagination = { current_page, total_pages, per_page, total };
      const data = _get(resp, 'data') || [];

      dispatch(storeBalanceHistoryData({ data, pagination }));
    }
  };

  const onFilterChange = () => {
    const { search, account_number_type, transaction_type, status } = state;
    setQueryParams((prev) => ({
      ...prev,
      page: 1,
      search,
      account_number_type,
      transaction_type,
      status,
    }));
  };

  const onFilterReset = () => {
    setState((prev) => ({
      ...prev,
      search: '',
      account_number_type: '',
      transaction_type: '',
      status: '',
    }))

    setQueryParams((prev) => ({
      ...prev,
      page: 1,
      search: '',
      account_number_type: '',
      transaction_type: '',
      status: '',
    }));
  }

  const onPageChange = (page) => {
    setQueryParams((prev) => ({ ...prev, page }));
  };

  useEffect(() => {
    (async () => {
      setPageLoading(true);

      const resp = await Promise.all([
        fetchBalance(),
        fetchBalanceHistories(queryParams)
      ]);

      setPageLoading(false);
    })();
  }, [queryParams]);

  return (
    <Container className='mb-4 pt-4'>
      <Row className='mb-1'>
        <Col sm='12' md='12' lg='12' className='d-flex'>
          <Form.Control
            type='text'
            className='mr-2'
            value={state.search}
            name='search'
            placeholder={t('search')}
            onChange={(e) => {
              setState((prev) => ({ ...prev, search: e.target.value }));
            }}
          >
          </Form.Control>

          <Form.Control
            as='select'
            className='mr-2'
            value={state.account_number_type}
            onChange={(e) => {
              setState((prev) => ({ ...prev, account_number_type: e.target.value }));
            }}
          >
            <option value=''>{t('account_number_type')}</option>
            {ACCOUNT_NUMBER_TYPE_OPTIONS.map((val, idx) => (
              <option key={`ant${idx}`} value={val.id}>
                {val.value}
              </option>
            ))}
          </Form.Control>

          <Form.Control
            as='select'
            className='mr-2'
            value={state.transaction_type}
            onChange={(e) => {
              setState((prev) => ({ ...prev, transaction_type: e.target.value }));
            }}
          >
            <option value=''>{t('transaction_type')}</option>
            {TRANSACTION_TYPE_OPTIONS.map((val, idx) => (
              <option key={`tp${idx}`} value={val.id}>
                {val.value}
              </option>
            ))}
          </Form.Control>
          <Form.Control
            as='select'
            value={state.status}
            onChange={(e) => {
              setState((prev) => ({ ...prev, status: e.target.value }));
            }}
          >
            <option value=''>{t('status')}</option>
            {STATUS_OPTIONS.map((val, idx) => (
              <option key={`st${idx}`} value={val.id}>
                {val.value}
              </option>
            ))}
          </Form.Control>
        </Col>
      </Row>

      <Row className='mb-4'>
        <Col sm='12' md='12' lg='12' className='text-right'>
          <Button type='button' variant='secondary' className='mr-2' onClick={onFilterReset}>
            {t('button.reset')}
          </Button>

          <Button type='button' variant='primary' onClick={onFilterChange}>
            {t('button.filter')}
          </Button>
        </Col>
      </Row>

      <PermissionRole allowed={[ROLE_USER]}>
        <Row className='mb-4'>
          <Col>
            <Card>
              <Card.Body>
                <h4 className='text-secondary'>{t('balance')}:</h4>
                <h3>{toIDR(amount)}</h3>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </PermissionRole>

      <Row className='mb-4'>
        <Col>
          <Card>
            <Card.Body>
              {!pageLoading && data.length > 0
                ? data.map((item, idx) => {
                    return (
                      <ItemDetail
                        key={`dt${idx}`}
                        data={item}
                      />
                    );
                  })
                : null}

              {pageLoading ? (
                <div className='text-center my-4'>
                  <Spinner animation='border' variant='primary' />
                </div>
              ) : null}
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <Row>
        <Col sm='12' md='12' lg='6'>
          {`Halaman ${pagination.current_page} dari ${pagination.total_pages}`}
        </Col>

        <Col sm='12' md='12' lg='6' className='text-right'>
          <SinglePagination
            current_page={pagination.current_page}
            total_page={pagination.total_pages}
            onChange={onPageChange}
          />
        </Col>
      </Row>
    </Container>
  );
};

export default Balance;
