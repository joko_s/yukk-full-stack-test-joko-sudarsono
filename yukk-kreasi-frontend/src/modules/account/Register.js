import _get from 'lodash/get';
import styled from '@emotion/styled';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Row, Col, Card, Form, Button, Spinner, InputGroup, FormControl } from 'react-bootstrap';

import { userRegister } from 'services/account';

import { isFieldError, getErrorMessage } from 'utils/formHelper';
import { FormInvalidFeedback } from 'components/common/form';

const Register = (props) => {
  const [errMessages, setErrMessages] = useState({});
  const [formLoading, setFormLoading] = useState(false);

  const navigate = useNavigate();
  const { t } = useTranslation();
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
  } = useForm();

  const customFormValidation = (values) => {
    let errors = [];
    if (values.phone_number && isValidPhoneNumber(values.phone_number)) {
      errors = errors.concat(`${t('form.no_hp')} tidak boleh dimulai angka '0'!`);
    }

    if (values.alt_phone_number && isValidPhoneNumber(values.alt_phone_number)) {
      errors = errors.concat(`${t('form.alt_no_telp')} tidak boleh dimulai angka '0'!`);
    }

    return errors;
  };

  const isValidPhoneNumber = (phone) => {
    const firstNumb = Array.from(phone)[0];
    return [0, '0'].includes(firstNumb);
  };

  const onSubmit = async (values) => {
    setErrMessages({});
    const validate = customFormValidation(values);
    if (validate.length > 0) {
      alert(validate.join('\n'));
      return;
    }

    setFormLoading(true);
    const resp = await userRegister(values);
    setFormLoading(false);

    if (resp.success) {
      reset();
      toast.success(t('login_success'));

      setTimeout(() => {
        navigate('/');
      }, 500);
    } else {
      const err = _get(resp, 'data.messages') || {};
      setErrMessages(err);

      const errMsg = _get(resp, 'data.messages.0') || t('register_failed');
      toast.error(errMsg);
    }
  };

  return (
    <Container>
      <Row className='justify-content-md-center'>
        <Col md='6' lg='4'>
          <Card style={{ marginTop: '120px', marginBottom: '120px' }}>
            <Card.Body className='text-center'>
              <Form onSubmit={handleSubmit(onSubmit)} className='text-left'>
                <Form.Group className='mb-3'>
                  <Form.Control type='email' placeholder={t('form.email')} {...register('email')} />

                  <FormInvalidFeedback isInvalid={isFieldError('email', errors, errMessages)}>
                    {getErrorMessage('email', errors, errMessages)}
                  </FormInvalidFeedback>
                </Form.Group>
                <Form.Group className='mb-3'>
                  <Form.Control type='text' placeholder={t('form.fullname')} {...register('name')} />

                  <FormInvalidFeedback isInvalid={isFieldError('name', errors, errMessages)}>
                    {getErrorMessage('name', errors, errMessages)}
                  </FormInvalidFeedback>
                </Form.Group>
                <Form.Group className='mb-3'>
                  <Form.Control type='password' placeholder={t('form.password')} {...register('password')} />

                  <FormInvalidFeedback isInvalid={isFieldError('password', errors, errMessages)}>
                    {getErrorMessage('password', errors, errMessages)}
                  </FormInvalidFeedback>
                </Form.Group>

                <Form.Group className='mb-3'>
                  <InputGroup>
                    <InputGroup.Prepend>
                      <InputGroup.Text>62</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl type='text' placeholder='85********' {...register('phone_number')} />
                  </InputGroup>

                  <FormInvalidFeedback isInvalid={isFieldError('phone_number', errors, errMessages)}>
                    {getErrorMessage('phone_number', errors, errMessages)}
                  </FormInvalidFeedback>
                </Form.Group>

                <Button type='submit' variant='primary' block>
                  {formLoading ? <Spinner animation='border' variant='light' size='sm' /> : null}{' '}
                  {t('button.register')}
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default Register;
