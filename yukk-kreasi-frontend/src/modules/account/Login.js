import _get from 'lodash/get';
import styled from '@emotion/styled';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Row, Col, Card, Form, Button, Spinner } from 'react-bootstrap';

import { setUserTokenStorage, setUserDataStorage } from 'utils/localStorage';
import { storeAccountProfile } from 'store/actions/account';
import { userLogin } from 'services/account';

const Separator = styled.div`
  width: 100%;
  text-align: center;
  border-bottom: 1px solid #333333;
  line-height: 0.1em;
  margin: 20px 0 20px;

  & span {
    background: #fff;
    padding: 0 10px;
  }
`;

const Login = (props) => {
  const [formLoading, setFormLoading] = useState(false);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
  } = useForm();

  const onSubmit = async (values) => {
    setFormLoading(true);
    const resp = await userLogin(values);
    const userDetail = _get(resp, 'data.user') || {};
    const token = _get(resp, 'data.token') || null;
    setFormLoading(false);

    if (resp.success && token) {
      // set token
      setUserTokenStorage(token);
      setUserDataStorage(userDetail);
      dispatch(storeAccountProfile(userDetail));

      reset();
      toast.success(t('login_success'));

      setTimeout(() => {
        navigate('/balance');
      }, 500);
    } else {
      const errMsg = _get(resp, 'data.messages.0') || t('login_failed');
      toast.error(errMsg);
    }
  };

  return (
    <Container>
      <Row className='justify-content-md-center'>
        <Col md='6' lg='4'>
          <Card style={{ marginTop: '120px', marginBottom: '120px' }}>
            <Card.Body className='text-center'>

              <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className='mb-3' controlId='exampleForm.ControlInput1'>
                  <Form.Control type='email' placeholder={t('form.email')} {...register('email')} />
                </Form.Group>
                <Form.Group className='mb-3' controlId='exampleForm.ControlTextarea1'>
                  <Form.Control type='password' placeholder={t('form.password')} {...register('password')} />
                </Form.Group>

                <Button type='submit' variant='primary' block>
                  {formLoading ? <Spinner animation='border' variant='light' size='sm' /> : null} {t('button.login')}
                </Button>

                <Separator>
                  <span>{t('or')}</span>
                </Separator>

                <Button type='button' variant='primary' block onClick={() => navigate('/register')}>
                  {t('button.create_new_account')}
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default Login;
