import { lazy, Suspense } from 'react';
import { AdminLayout } from 'components/themes/private';

const TransactionDetail = lazy(() => import('modules/transaction/TransactionDetail'));

const TransactionDetailPage = (props) => (
  <AdminLayout>
    <Suspense fallback={null}>
      <TransactionDetail />
    </Suspense>
  </AdminLayout>
);

export default TransactionDetailPage;
