import { lazy, Suspense } from 'react';
import { AdminLayout } from 'components/themes/private';

const Transaction = lazy(() => import('modules/transaction/Transaction'));

const TransactionPage = (props) => (
  <AdminLayout>
    <Suspense fallback={null}>
      <Transaction />
    </Suspense>
  </AdminLayout>
);

export default TransactionPage;
