import { lazy, Suspense } from 'react';
import { AdminLayout } from 'components/themes/private';

const CreateTransaction = lazy(() => import('modules/transaction/CreateTransaction'));

const CreateTransactionPage = (props) => (
  <AdminLayout>
    <Suspense fallback={null}>
      <CreateTransaction />
    </Suspense>
  </AdminLayout>
);

export default CreateTransactionPage;
