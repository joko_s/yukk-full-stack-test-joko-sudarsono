import { lazy, Suspense } from 'react';
import { AdminLayout } from 'components/themes/private';

const Balance = lazy(() => import('modules/balance/Balance'));

const BalancePage = (props) => (
  <AdminLayout>
    <Suspense fallback={null}>
      <Balance />
    </Suspense>
  </AdminLayout>
);

export default BalancePage;
