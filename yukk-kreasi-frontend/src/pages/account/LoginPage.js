import { lazy, Suspense } from 'react';
import { Layout } from 'components/themes/v1';

const Login = lazy(() => import('modules/account/Login'));

const LoginPage = (props) => (
  <Layout>
    <Suspense fallback={null}>
      <Login />
    </Suspense>
  </Layout>
);

export default LoginPage;
