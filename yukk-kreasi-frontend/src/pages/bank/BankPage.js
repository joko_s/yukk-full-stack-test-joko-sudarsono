import { lazy, Suspense } from 'react';
import { AdminLayout } from 'components/themes/private';

const Bank = lazy(() => import('modules/bank/Bank'));

const BankPage = (props) => (
  <AdminLayout>
    <Suspense fallback={null}>
      <Bank />
    </Suspense>
  </AdminLayout>
);

export default BankPage;
