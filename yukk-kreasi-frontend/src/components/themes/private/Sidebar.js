import _get from 'lodash/get';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { ProSidebar, Menu, MenuItem, SubMenu } from 'react-pro-sidebar';

import UserDummy from 'assets/images/user-dummy.jpg';

const SidebarWrapper = styled.div`
  width: 245px;
  background-color: #fff;
  position: fixed;
  left: 0;
  top: 0;
  bottom: 0;
  overflow-y: auto;
  z-index: 1040;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);

  & .pro-sidebar .pro-menu .pro-menu-item {
    font-size: 12px;
  }
`;

const SidebarHeader = styled.div`
  display: block;
  position: relative;
  width: 100%;
  padding: 36px 12px;
  text-align: center;
`;

const SidebarMenu = styled.div`
  width: 100%;
`;

const UserImage = styled.img`
  display: block;
  width: 70px;
  height: 70px;
  margin: auto 12px auto 0;
  border-radius: ${(props) => (props.circle ? '50%' : 0)};
`;

const Sidebar = (props) => {
  const navigate = useNavigate();
  const { t } = useTranslation();

  return (
    <SidebarWrapper className='shadow-3'>
      <SidebarHeader>
        <UserImage circle src={UserDummy} alt='-' />
      </SidebarHeader>

      <SidebarMenu>
        <ProSidebar>
          <Menu iconShape='square'>
            {/* <MenuItem>
              {t('menu.bank')}
              <Link to='/bank' />
            </MenuItem> */}

            <MenuItem>
              {t('menu.balance')}
              <Link to='/balance' />
            </MenuItem>

            <MenuItem>
              {t('menu.transaction')}
              <Link to='/transaction' />
            </MenuItem>
          </Menu>
        </ProSidebar>
      </SidebarMenu>
    </SidebarWrapper>
  );
};

export default Sidebar;
