import _get from 'lodash/get';
import styled from '@emotion/styled';
import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate, Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Navbar, Nav, NavDropdown, Modal, Button, Spinner } from 'react-bootstrap';
import { BellFill, CloudArrowDownFill, BoxArrowRight } from 'react-bootstrap-icons';

import { userLogout } from 'services/account';
import { clearUserTokenStorage, clearUserDataStorage } from 'utils/localStorage';

const NavbarIcon = styled.span`
  display: inline-block;
  position: relative;
  font-size: 1.25rem;
`;

const AdminHeader = (props) => {
  const [modalLogout, setModalLogout] = useState(false);
  const [logoutLoading, setLogoutLoading] = useState(false);

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const onLogout = async () => {
    setLogoutLoading(true);
    const resp = await userLogout();
    setLogoutLoading(false);

    if (resp.success) {
      // clear token & data
      clearUserTokenStorage();
      clearUserDataStorage();

      toast.success(t('logout_success'));

      setTimeout(() => {
        window.location.href = '/';
      }, 500);
    } else {
      toast.error(t('http.request_failed'));
    }
  };

  return (
    <Navbar fixed='top' bg='light' expand='lg' className='navbar-private' style={{ paddingLeft: '250px' }}>
      <Navbar.Toggle aria-controls='basic-navbar-nav' />
      <Navbar.Collapse id='basic-navbar-nav'>
        <Nav className='mr-auto' />
        <Nav>
          <Nav.Link className='p-0 mr-3' onClick={() => setModalLogout(true)}>
            <NavbarIcon>
              <BoxArrowRight />
            </NavbarIcon>
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>

      <Modal show={modalLogout} onHide={() => setModalLogout(false)}>
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body className='text-center pb-4'>
          {t('are_you_sure_to_logout')}

          <div className='mt-3'>
            <Button variant='primary' onClick={() => onLogout()} className='mr-3'>
              {logoutLoading ? <Spinner animation='border' variant='light' size='sm' /> : null} {t('button.yes')}
            </Button>
            <Button variant='secondary' onClick={() => setModalLogout(false)}>
              {t('button.no')}
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </Navbar>
  );
};

export default AdminHeader;
