import _get from 'lodash/get';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';

import { setLang, getUserTokenStorage } from 'utils/localStorage';
import { storeLang } from 'store/actions/lang';

const langOpt = {
  id: 'lang.indonesia',
  en: 'lang.english',
};

const Header = (props) => {
  const userToken = getUserTokenStorage();
  const lang = useSelector((state) => state.lang);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t, i18n } = useTranslation();

  const onLangChange = (lng) => {
    dispatch(storeLang(lng));
    i18n.changeLanguage(lng);
    setLang(lng);
  };

  return (
    <Navbar fixed='top' bg='light' expand='lg' className='navbar-public'>
      <Container fluid>
        <Navbar.Brand className='cursor-pointer' onClick={() => navigate('/')}>
          Brand
        </Navbar.Brand>
        <Navbar.Toggle aria-controls='basic-navbar-nav' />
        <Navbar.Collapse id='basic-navbar-nav'>
          <Nav className='mr-auto' />
          <Nav>
            <Nav.Link className='text-uppercase' onClick={() => navigate('/')}>
              {t('menu.login')}
            </Nav.Link>
            <Nav.Link className='text-uppercase' onClick={() => navigate('/register')}>
              {t('menu.register')}
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
