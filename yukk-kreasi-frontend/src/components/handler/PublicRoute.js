import { useLocation, Navigate } from 'react-router-dom';

import { getUserTokenStorage } from 'utils/localStorage';

const PublicRoute = ({ children }) => {
  const location = useLocation();
  const token = getUserTokenStorage();

  if (token) {
    return <Navigate to='/balance' state={{ from: location }} replace />;
  }

  return children;
};

export default PublicRoute;
