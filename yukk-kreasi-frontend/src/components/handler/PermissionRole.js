import _get from 'lodash/get';
import { useSelector } from 'react-redux';

/**
 * this component is to handle show or hide components by user role
 * eg: action button, sensitive information
 *
 * example usage:
 * <PermissionRole allowed={[ROLE_SUPERADMIN]}>
 *   <Button type='button' variant='primary' onClick={editData}>
 *     {t('button.edit')}
 *   </Button>
 * </PermissionRole>
 */
const PermissionRole = ({ children, allowed }) => {
  const userData = useSelector((state) => state.account.detail);
  const roleId = _get(userData, 'user_role') || '';

  if (!allowed.includes(roleId)) {
    return <></>;
  }

  return children;
};

export default PermissionRole;
