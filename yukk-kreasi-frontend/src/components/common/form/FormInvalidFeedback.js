import styled from '@emotion/styled';

const FormInvalidFeedback = styled.div`
  display: ${(props) => (props.isInvalid ? 'block' : 'none')};
  width: 100%;
  margin-top: 0.25rem;
  font-size: 80%;
  color: #e52128;
`;

export default FormInvalidFeedback;
