import _get from 'lodash/get';
import { useTranslation } from 'react-i18next';
import { Form } from 'react-bootstrap';

const BasicSelect = ({
  id,
  style = {},
  className = '',
  options,
  placeholder,
  value,
  keys,
  isInvalid,
  translateName,
  onChange,
}) => {
  const idKey = _get(keys, 'id') || 'id';
  const nameKey = _get(keys, 'name') || 'name';

  const { t } = useTranslation();

  return (
    <Form.Control
      style={style}
      as='select'
      className={className}
      value={value ? value : ''}
      isInvalid={isInvalid}
      onChange={(e) => onChange(e.target.value)}
    >
      <option value=''>{placeholder}</option>

      {options.map((val, idx) => (
        <option key={`${id}${idx}`} value={val[idKey]}>
          {translateName ? t(val[nameKey]) : val[nameKey]}
        </option>
      ))}
    </Form.Control>
  );
};

export default BasicSelect;
