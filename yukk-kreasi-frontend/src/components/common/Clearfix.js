import styled from '@emotion/styled';

const Clearfix = styled.div`
  &:after {
    display: block;
    clear: both;
    content: '';
  }
`;

export default Clearfix;
