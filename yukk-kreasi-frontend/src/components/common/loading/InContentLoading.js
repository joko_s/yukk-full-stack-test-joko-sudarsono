import styled from '@emotion/styled';
import { Container, Row, Col, Spinner } from 'react-bootstrap';

const LoaderWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 150px;
`;

const Text = styled.span`
  color: #4e4e4e;
  display: inline-block;
  font-size: 14px;
  margin-left: 12px;
  margin-top: 4px;
`;

const InContentLoading = ({ height, size, textLoading }) => (
  <Row className='justify-content-md-center'>
    <Col style={{ textAlign: 'center' }}>
      <LoaderWrapper style={{ height }}>
        <Spinner animation='border' variant='secondary' size={size} />
        <Text>{textLoading}</Text>
      </LoaderWrapper>
    </Col>
  </Row>
);

export default InContentLoading;
