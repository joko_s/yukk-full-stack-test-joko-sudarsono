import { Container } from 'react-bootstrap';
import { InContentLoading } from '.';

const PageLoading = (props) => (
  <Container style={{ marginTop: '80px' }}>
    <InContentLoading {...props} />
  </Container>
);

export default PageLoading;
